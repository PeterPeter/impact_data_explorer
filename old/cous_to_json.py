import pandas as pd
import json
import numpy as np

import pycountry


possible_paths = [
	'/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/',
	'/home/tooli/impact_data_explorer_api/data/'
]

for data_path in possible_paths:
	if os.path.isdir(data_path):
		break

out = {}
out['countries'] = []
for cou in pycountry.countries:
	iso = cou.alpha_3
	if os.path.isdir(data_path + 'cou_data/'+iso) and os.path.isfile(data_path + 'topojsons/'+iso+'_0_topo.json'):
		sub_regions = [iso]
		if os.path.isfile(data_path+'/topojsons/'+iso+'_1_topo.json'):
			with open(data_path+'/topojsons/'+iso+'_1_topo.json') as f:
				sub = json.load(f)
			for reg in sub['objects']['data']['geometries']:
				sub_regions.append(reg['properties']['HASC_1'])

		out['countries'].append(
			{
				'id' : iso,
				'name' : cou.name,
				'sub_regions' : sub_regions,
			}
		)










#
