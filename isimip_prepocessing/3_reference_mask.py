import os,sys, importlib, glob,gc, operator

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader
import statsmodels.formula.api as smf


import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['dis','mindis','maxdis','qs'])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

# dict { season name : list(months) }
season_dict = {
	'annual' : list(range(1,13)),
	'DJF' : [1,2,12],
	'MAM' : [3,4,5],
	'JJA' : [6,7,8],
	'SON' : [9,10,11],
}


for var_orig in args['vars']:
    print(var_orig)

    # load details about the variable
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var_orig+'_check']*3))
    details_orig = details

    if 'ref_mask' in details_orig.keys():
        var = details_orig['ref_mask']['var']
        # load details about the variable
        sys.path.append('meta/variables/')
        exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))
        
        if 'excludeImpModels' in list(details.keys()):
            exludeImp = details['excludeImpModels']
        else:
            exludeImp = []

        # here I load reference slices
        # I load them several times - once for each RCP
        # I need this to be able to easily calculate changes later on
        files_all = glob.glob(details['wlvl_files_pattern'])
        files = [f for f in files_all if '1986-2006' in f]    
        # filter out excluded imp models
        files = [f for f in files if len([i for i in exludeImp if i in f]) == 0]
        # IDs are all the information about a slice except the warming level (gcm, rcp, impModel)
        IDs = [f.split('/')[-1].replace('_'+'1986-2006'+'.nc','').replace('_'+var,'') for f in files]

        # get files for 1.5°C to check which models are excluded
        files_all = glob.glob(details['wlvl_files_pattern'])
        files_proj = [f for f in files_all if '1.5' in f]
        # filter out excluded imp models
        files_proj = [f for f in files_proj if len([i for i in exludeImp if i in f]) == 0]
        proj_IDs = [f.split('/')[-1].replace('_1.5.nc','').replace('_'+var,'').replace('_rcp26','').replace('_rcp45','').replace('_rcp60','').replace('_rcp85','') for f in files_proj]

        # here I make a selection of IDs that are available in ref and proj
        IDs = [id_ for id_ in IDs if id_.replace('_historical','') in proj_IDs]

        # load only data that is in IDs
        datas = []
        for fl in files:
            if fl.split('/')[-1].replace('_'+'1986-2006'+'.nc','').replace('_'+var,'') in IDs:
                datas.append(xr.load_dataset(fl)[var])
        ref = xr.concat(datas, dim='ID')
        ref = ref.assign_coords(ID=IDs)

        ref = ref.loc[IDs]

        # load the data into the RAM
        ref.load()

        # this is a correction required for some specific data (led, ped, .. etc.)
        if 'correction_factor' in details.keys():
            ref *= float(details['correction_factor'])

        # prep mask
        ds = xr.Dataset()
        ds.attrs = {'IDs':IDs, 'mask':str(details_orig['ref_mask'])}
        
        for season,months in season_dict.items():
            if details['temporal_resolution'] != 'yearly' or season == 'annual':
                mask = ref.loc[:,months].reduce(details['seasonal_aggreagation'], 'month').median('ID')
                mask.values[details_orig['ref_mask']['operator'](mask.values, details_orig['ref_mask']['threshold'])] = np.nan
                mask.values[np.isfinite(mask)] = 1 
                ds[season] = mask

        ds.to_netcdf('/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var_orig+'/'+'_'.join([var_orig])+'_ref_mask.nc')








