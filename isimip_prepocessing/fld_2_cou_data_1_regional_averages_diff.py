import os, sys, importlib, glob, gc, operator

import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import geopandas as gpd

import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

# seasons and months
season_dict = {
    'annual': list(range(1, 13)),
    'DJF': [1, 2, 12],
    'MAM': [3, 4, 5],
    'JJA': [6, 7, 8],
    'SON': [9, 10, 11],
}

# for w1 in 1 2 3 4 5; do for w2 in 0 1 2 3 4 5 6 7 8 9 ; do for var in dis hursAdjust lec lef lew mindis ped peh prAdjust psAdjust qs rsdsAdjust snd tasAdjust tasminAdjust yield_maize_co2 yield_soy_co2 ec1 flddph_720x360 fldfrc_720x360 hussAdjust led leh maxdis pec pef pew prsnAdjust pslAdjust rldsAdjust sfcWindAdjust soilmoist tasmaxAdjust thawdepth yield_rice_co2 yield_wheat_co2; do sbatch job_single.sh isimip_prepocessing/2_cou_data_1_regional_averages_diff.py --vars $var --wlvls ${w1}.${w2} --isos SSD ESH AFRICA ASIA EUROPE NORTH_AMERICA SOUTH_AMERICA OCEANIA; done; done; done;

# here the command line arguments are analyzed
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--isos', help='isos', nargs='+', default=[])
parser.add_argument('-v', '--vars', help='variables', nargs='+', default=['fldfrc'])
parser.add_argument('-s', '--seasons', help='seasons', nargs='+', default=['annual'])

parser.add_argument('-w', '--wlvls', help='warming levels', nargs='+',
                    default=[str(w) for w in np.arange(1.0, 5.1, 0.1).round(1)])
parser.add_argument('-r', '--ref', help='reference warming level', default='1.0', type=str)

parser.add_argument('--subregions', dest='subregions', action='store_true')
parser.add_argument('--no-subregions', dest='subregions', action='store_false')
parser.set_defaults(subregions=True)

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)
details = {'overwrite': 'no',
           'long_name': 'Land area annually exposed to river floods (Average)',
           'orig_name': 'fldfrc',
           'short_name': 'fldfrc',
           'source': 'ISIMIP - Extreme Events',
           'rel-abs': 'relative',
           'temporal_resolution': 'yearly',
           'wlvl_files_pattern': '/mnt/PROVIDE/niklas/fldfrc_reproduction/raw/fldfrc/*/*/*/*',
           'in_files': glob.glob('/p/projects/isimip/isimip/inga/ngfs/flood_data/*/flddph*')
           }

# in case no iso was specified -> take all isos
if len(args['isos']) == 0:
    gdf = gpd.read_file('/mnt/PROVIDE/niklas/extremerainfall/data/world/ne_50m_admin_0_countries.shp')
    isos = list(gdf['adm0_a3'].unique())
else:
    isos = args['isos']

# this is goinf to be initialized with the first wlvl
iso_dict = {}

for var in args['vars']:
    print(var)

    # load details about the variable
    # sys.path.append('meta/variables/')
    # exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    # load reference period masks if required
    if 'ref_mask' in details.keys():
        ref_mask_nc = xr.load_dataset(
            '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/' + var + '/' + '_'.join([var]) + '_ref_mask.nc')

    # in this array I will store deviations from the median
    if details['temporal_resolution'] == 'yearly':
        seasons = ['annual']
    else:
        seasons = args['seasons']

    if 'excludeImpModels' in list(details.keys()):
        exludeImp = details['excludeImpModels']
    else:
        exludeImp = []

    # here I load reference slices
    if len([fl for fl in glob.glob(details['wlvl_files_pattern']) if '1986-2006' in fl]) > 0:
        # I load them several times - once for each RCP
        # I need this to be able to easily calculate changes later on
        print([fl for fl in glob.glob(details['wlvl_files_pattern']) if '1986-2006' in fl])
        files_all = glob.glob(details['wlvl_files_pattern'])
        files = [f for f in files_all if '1986-2006' in f]
        # filter out excluded imp models
        files = [f for f in files if len([i for i in exludeImp if i in f]) == 0]
        ref = []
        for rcp in ['rcp26', 'rcp45', 'rcp60', 'rcp85']:
            # IDs are all the information about a slice except the warming level (gcm, rcp, impModel)
            IDs = [f.split('/')[-1].replace('_' + '1986-2006' + '.nc', '').replace('_' + var, '').replace('historical',
                                                                                                          rcp) for f in
                   files]
            tmp = xr.open_mfdataset(files, concat_dim='ID')[var]  # .sel({'lat':lats, 'lon':lons})
            tmp = tmp.assign_coords(ID=IDs)
            ref.append(tmp)
        ref = xr.concat(ref, dim='ID')

    else:
        files_all = glob.glob(details['wlvl_files_pattern'])

        files = [f for f in files_all if args['ref'] in f]
        # filter out excluded imp models
        files = [f for f in files if len([i for i in exludeImp if i in f]) == 0]
        IDs = [f.split('/')[-1].replace('_' + args['ref'] + '.nc', '').replace('_' + var, '') for f in files]

        ref = xr.open_mfdataset(files, concat_dim='ID', combine='nested')[var]
        ref = ref.assign_coords(ID=IDs)

    # load the data into the RAM
    ref = ref.load().copy()

    # this is a correction required for some specific data (led, ped, .. etc.)
    if 'correction_factor' in details.keys():
        ref *= float(details['correction_factor'])

    for wlvl in args['wlvls']:
        # this might free RAM during the loops
        gc.collect()
        print(wlvl)

        # this is just for me to check progress
        # with open('progress/'+var+'_cou.txt', 'w') as fl_:
        print(f'progress: {wlvl}')

        # here I load all files for the warming level
        files_all = glob.glob(details['wlvl_files_pattern'])
        files = [f for f in files_all if wlvl in f]
        # filter out excluded imp models
        files = [f for f in files if len([i for i in exludeImp if i in f]) == 0]
        if len(files) > 0:
            IDs = [f.split('/')[-1].replace('_' + wlvl + '.nc', '').replace('_' + var, '') for f in files]
            proj = xr.open_mfdataset(files, concat_dim='ID', combine='nested')[var]  # .sel({'lat':lats, 'lon':lons})
            proj = proj.assign_coords(ID=IDs)

            # load into RAM
            proj = proj.load().copy()

            # this is a correction required for some specific data (led, ped, .. etc.)
            if 'correction_factor' in details.keys():
                proj *= float(details['correction_factor'])

            # here I make a selection of IDs that are available in ref and proj
            IDs = ref.ID.values[np.isin(ref.ID, proj.ID)]

            # remove infs
            ref.values[np.isfinite(ref.values) == False] = np.nan
            proj.values[np.isfinite(proj.values) == False] = np.nan

            # IDs_gcm_im -> same length as IDs but with rcp information omitted
            # IDs_gcm_im_unique -> removed duplicates in IDs_gcm_im
            IDs_gcm_im = np.array(['_'.join([i for i in id_.split('_') if 'rcp' not in i]) for id_ in IDs])
            IDs_gcm_im_unique = np.unique(IDs_gcm_im)

            for season in seasons:
                # load reference period masks if required
                if 'ref_mask' in details.keys():
                    ref_mask = ref_mask_nc[season]
                    ref_ = ref * ref_mask
                    proj_ = proj * ref_mask

                else:
                    ref_ = ref
                    proj_ = proj

                # go through countries - somehow I call them isos because I work with 3 letter country codes
                for iso in isos:
                    if len(glob.glob('/mnt/PROVIDE/niklas/extremerainfall/data/masks/' + iso + '/masks/*')) > 0:
                        print(iso)
                        # create output path it does not exist
                        path_out = '/mnt/PROVIDE/niklas/fldfrc_reproduction/output/timeseries_raw/' + var + '/' + iso + '/'
                        os.system('mkdir -p ' + path_out)

                        # define output file name and only continue if that file does not exist or the option is --overwrite
                        file_out = path_out + '_'.join([iso, var, wlvl]) + '_regAv.nc'
                        if os.path.isfile(file_out) == False or args['overwrite']:

                            # the following block only happens for the first warming level
                            # it loads masks, prepares output arrays etc.
                            if iso not in iso_dict.keys():
                                # load masks
                                mask_dict = {
                                    'area': xr.open_dataset(
                                        f'/mnt/PROVIDE/niklas/extremerainfall/data/masks/{iso}/masks/{iso}_360x720lat89p75to-89p75lon-179p75to179p75_latWeight.nc4'),
                                    'pop': xr.open_dataset(
                                        f'/mnt/PROVIDE/niklas/extremerainfall/data/masks/{iso}/masks/{iso}_360x720lat89p75to-89p75lon-179p75to179p75_pop2005.nc4'),
                                    'gdp': xr.open_dataset(
                                        f'/mnt/PROVIDE/niklas/extremerainfall/data/masks/{iso}/masks/{iso}_360x720lat89p75to-89p75lon-179p75to179p75_gdp2005.nc4'),
                                }
                                # I'll take lons and lats from this mask
                                mask_overlap = xr.open_dataset(
                                    f'/mnt/PROVIDE/niklas/extremerainfall/data/masks/{iso}/masks/{iso}_360x720lat89p75to-89p75lon-179p75to179p75_overlap.nc4')

                                # depending on --subregions or --no-subregions the region names are loaded
                                if args['subregions']:

                                    region_names = list(mask_overlap.data_vars)

                                else:
                                    region_names = [iso]

                                # all details are stored in the iso_dict
                                iso_dict[iso] = {
                                    'lats': mask_overlap.coords['lat'].values,
                                    'lons': mask_overlap.coords['lon'].values,
                                    'mask_dict': mask_dict,
                                    'region_names': region_names
                                }

                            # get details for the iso
                            iso_details = iso_dict[iso]

                            # define output array
                            if os.path.isfile(file_out):
                                diff_out = xr.load_dataset(file_out)['diff']
                            else:
                                diff_out = xr.DataArray(coords={'region': iso_details['region_names'],
                                                                'aggregation': list(iso_details['mask_dict'].keys()),
                                                                'season': seasons, 'ID': IDs_gcm_im_unique},
                                                        dims=['region', 'aggregation', 'season', 'ID'])

                            for aggr, masks in iso_details['mask_dict'].items():
                                for region in iso_details['region_names']:

                                    # select mask

                                    #mask = masks.loc[region]
                                    mask = masks[region]

                                    print(mask)

                                    # average over area
                                    ref_area = (ref_.copy().sel(ID=IDs).sel(
                                        {'lat': iso_details['lats'], 'lon': iso_details['lons']}) * mask).sum(
                                        'lat').sum('lon')
                                    proj_area = (proj_.sel(ID=IDs).sel(
                                        {'lat': iso_details['lats'], 'lon': iso_details['lons']}) * mask).sum(
                                        'lat').sum('lon')

                                    # print(np.sum(ref_area != 0))

                                    if details['temporal_resolution'] == 'yearly':
                                        ref_area_seas, proj_area_seas = ref_area, proj_area
                                    else:
                                        months = season_dict[season]
                                        # select months of season and aggregate depending on the "seasonal_aggregation" in details
                                        ref_area_seas = ref_area.loc[:, months].reduce(details['seasonal_aggreagation'],
                                                                                       'month')
                                        proj_area_seas = proj_area.loc[:, months].reduce(
                                            details['seasonal_aggreagation'], 'month')

                                    # this is now the change in the the variable for a warming level for all different gcm-impModel-rcp combinations
                                    if details['rel-abs'] == 'relative':
                                        diff_all = (proj_area_seas - ref_area_seas) / ref_area_seas * 100
                                    if details['rel-abs'] == 'absolute':
                                        diff_all = proj_area_seas - ref_area_seas

                                    # load into RAM if it isn't already there
                                    diff_all = diff_all.load()

                                    # average over rcps
                                    gcm_im_combis = []
                                    for id_ in IDs_gcm_im_unique:
                                        gcm_im_combis.append(diff_all[IDs_gcm_im == id_].mean('ID'))
                                    diff = xr.concat(gcm_im_combis, dim='ID')
                                    diff = diff.assign_coords(ID=IDs_gcm_im_unique)

                                    try:
                                        diff_out.loc[region, aggr, season, diff.ID] = diff
                                    except:
                                        print('some issue here')
                                        print(region, aggr, season, diff.ID)
                                        print(diff)

                                    # clean infs
                                    diff_out.values[np.isfinite(diff_out.values) == False] = np.nan

                            # save
                            xr.Dataset({'diff': diff_out}).to_netcdf(file_out)
                            print('done', wlvl, iso, file_out)
                            gc.collect()