import os,sys, importlib, glob,gc
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import json

import cartopy.io.shapereader as shapereader
from shapely.ops import cascaded_union, unary_union
from shapely.geometry import mapping, Polygon, MultiPolygon, asShape, Point

import topojson as tp
import geopandas
import pandas as pd

from geojson_rewind import rewind

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

try:
	overwrite = sys.argv[1]
except:
	overwrite = False

cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
continents = np.unique([info.attributes['continent'] for info in cous])

example_grid = xr.open_dataset('/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/historical/HadGEM2-ES/tas_day_HadGEM2-ES_historical_r1i1p1_EWEMBI_18610101-18701231.nc4')['tas'][0]

pop2005 = xr.open_dataset('/p/projects/isimip/isimip/ISIMIP2b/InputData/population/2005soc/population_2005soc_0p5deg_annual_2006-2099.nc4', decode_times=False)['number_of_people'][0]
pop2005.squeeze().load()
pop2005 = xr.DataArray(pop2005.values, coords={k:pop2005.coords[k] for k in ['lat','lon']}, dims=['lat','lon'])

gdp2005 = xr.open_dataset('/p/projects/isimip/isimip/ISIMIP2b/InputData/gdp/2005soc/gdp_2005soc_0p5deg_annual_2006-2099.nc4', decode_times=False)['gdp'][0]
gdp2005.squeeze().load()
gdp2005 = xr.DataArray(gdp2005.values, coords={k:gdp2005.coords[k] for k in ['lat','lon']}, dims=['lat','lon'])


# continents = np.append(continents, ['GLOBAL','AMERICA'])
continents = ['Global','America']
for continent in continents:
    cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()

    if continent == 'Global':
        isos = list(np.unique([info.attributes['adm0_a3'] for info in cous])) + ['ESH','SSD']
    elif continent == 'America':
        isos = np.unique([info.attributes['adm0_a3'] for info in cous if info.attributes['continent'] in ['South America','North America']])
    else:
        isos = np.unique([info.attributes['adm0_a3'] for info in cous if info.attributes['continent']==continent])

    if continent == 'Europe':
        isos = [iso for iso in isos if iso != 'RUS']

    if continent == 'Africa':
        isos = list(isos) + ['ESH','SSD']

    continent = continent.upper().replace(' ','_').replace('(','').replace(')','')

    mask = xr.DataArray(0., coords={k:v for k,v in example_grid.coords.items() if k in ['lat','lon']}, dims=example_grid.dims)

    for iso in isos:
        files = glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*360x720*overlap.nc*')
        if len(files) > 0:
            fl = [fl for fl in files if 'simplify' not in fl][0]
            tmp = xr.open_dataset(fl)[iso]
            tmp.values[np.isnan(tmp.values)] = 0

            mask.loc[tmp.lat,tmp.lon] += tmp.values

    os.system('mkdir -p /p/tmp/pepflei/impact_data_explorer_data/masks/'+continent+'/masks/')

    mask.values[mask == 0] = np.nan

    # get relevant extend
    lons=sorted(np.where(np.isfinite(np.nanmean(mask,0)))[0])
    lon_ = list(mask.lon.values[lons[0]:lons[-1]+1])
    lats=sorted(np.where(np.isfinite(np.nanmean(mask,1)))[0])
    lat_ = list(mask.lat.values[lats[0]:lats[-1]+1])

    out_mask = mask.sel({'lat':lat_,'lon':lon_})

    nc_out_mask = xr.Dataset({continent:out_mask})

    for iso in isos:
        files = glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*360x720*overlap.nc*')
        if len(files) > 0:
            fl = [fl for fl in files if 'simplify' not in fl][0]
            tmp = xr.open_dataset(fl)[iso]
            tmp_mask = out_mask.copy() * 0.0
            tmp_mask.loc[tmp.lat,tmp.lon] += tmp.values * 4

            nc_out_mask[iso] = tmp_mask

    small_grid=str(str(len(lat_))+'x'+str(len(lon_))+'lat'+str(lat_[0])+'to'+str(lat_[-1])+'lon'+str(lon_[0])+'to'+str(lon_[-1])).replace('.','p')

    nc_out_mask.attrs['original_grid'] = xr.open_dataset(fl).attrs['original_grid']
    nc_out_mask.attrs['grid'] = small_grid
    nc_out_mask.attrs['mask_style'] = 'overlap'
    nc_out_mask.to_netcdf('/p/tmp/pepflei/impact_data_explorer_data/masks/'+continent+'/masks/'+continent+'_'+xr.open_dataset(fl).attrs['original_grid']+'_overlap.nc4')

    COU = class_on_shape.shaped_object(iso=continent, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+continent)
    COU.load_mask()
    COU.create_masks_latweighted()

    COU.create_masks_other_weight(pop2005, 'pop2005')

    COU.create_masks_other_weight(gdp2005, 'gdp2005')


    gc.collect()




def combineBorders(*geoms):
    return cascaded_union([
        geom if geom.is_valid else geom.buffer(0) for geom in geoms
    ])

# create topojsons
for continent in continents:
    print(continent)
    cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
    isos = np.unique([info.attributes['adm0_a3'] for info in cous if info.attributes['continent']==continent])
    if continent == 'Europe':
        isos = [iso for iso in isos if iso != 'RUS']

    cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
    first = True
    for item in cous:
        tmp,region=item.geometry,item.attributes


        if region['adm0_a3'] in isos:
            if first:
                shape = tmp
                first = False
            else:
                shape = combineBorders(shape,tmp)

    if continent == 'Africa':
        for iso in ['ESH','SSD']:
            tmp = next(shapereader.Reader('/p/tmp/pepflei/impact_data_explorer_data/masks/SSD/SSD_adm_shp/gadm36_SSD_0.shp').records()).geometry
            shape = cascaded_union((shape,tmp))

    continent = continent.upper().replace(' ','_').replace('(','').replace(')','')

    out_file = '/p/tmp/pepflei/impact_data_explorer_data/topojsons/'+continent+'_0_topo.json'    
    topo = rewind(tp.Topology(shape, prequantize=False).to_json())
    open(out_file, 'w').write(topo)


#
