import xarray as xr
import os
import requests

relevant_historical_years = ['19810101-19901231','19910101-20001231','20010101-20051231']

years  = ['20060101-20101231','20110101-20201231', '20210101-20301231', '20310101-20401231', '20410101-20501231', '20510101-20601231', '20610101-20701231', '20710101-20801231', '20810101-20901231', '20910101-20991231']
# Replace 'your_file.nc' with the path to your NetCDF file
file_pattern_historical = 'pr_day_{model}_historical_r1i1p1_EWEMBI_{year}.nc4'
file_pattern = 'pr_day_{model}_{scenario}_r1i1p1_EWEMBI_{year}.nc4'
models = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']
scenarios = ['rcp26','rcp45','rcp60','rcp85']

def download_file(file_url, path):

    # Create the directory if it doesn't exist
    os.makedirs(path, exist_ok=True)

    # Get the file name from the URL
    file_name = os.path.join(path, os.path.basename(file_url))

    # Send an HTTP GET request to the URL
    response = requests.get(file_url)

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Open the file in binary write mode and write the content
        with open(file_name, 'wb') as file:
            file.write(response.content)
        print(f"File '{os.path.basename(file_name)}' downloaded successfully to '{path}'")
    else:
        print(f"Failed to download file from '{file_url}'")

    # Close the response
    response.close()


def download_relevant_datasets(model, scenario, years):
    for relevant_year in years:
        link = f'https://files.isimip.org/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/{scenario}/{model}/pr_day_{model}_{scenario}_r1i1p1_EWEMBI_{relevant_year}.nc4'
        download_file(link, f'/mnt/PROVIDE/niklas/extremerainfall/input/{model}/{scenario}/')

#download_file('https://files.isimip.org/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/rcp26/GFDL-ESM2M/pr_day_GFDL-ESM2M_rcp26_r1i1p1_EWEMBI_20110101-20201231.nc4','/mnt/PROVIDE/niklas/extremerainfall/input/GFDL-ESM2M/rcp26/')
for model in models:
    download_relevant_datasets(model, 'historical', relevant_historical_years )
    for scenario in scenarios:
        download_relevant_datasets(model, scenario, years)



