import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import cartopy.io.shapereader as shapereader
import statsmodels.formula.api as smf

import matplotlib.pyplot as plt

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-i','--isos', help='isos', nargs='+', default=[])
# parser.add_argument('-v','--vars', help='variables', nargs='+', default=['flddph_720x360'])
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['dis','hursAdjust','lec','lef','lew','mindis','peh','prAdjust','psAdjust','qs','tasAdjust','tasminAdjust','yield_maize_co2','yield_soy_co2','ec1','flddph_720x360','fldfrc_720x360','hussAdjust','leh','maxdis','pec','pef','pew','prsnAdjust','pslAdjust','rldsAdjust','sfcWindAdjust','soilmoist','tasmaxAdjust','yield_rice_co2','yield_wheat_co2'])
parser.add_argument('-p','--plotCous', help='list of countries for which to create plots', nargs='+', default=['DEU','BEN','CHN'])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

# in case no iso was specified -> take all isos
if len(args['isos']) == 0:
    cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
    isos = [info.attributes['adm0_a3'] for info in cous]
else:
    isos = args['isos']

for var in args['vars']:
    print(var)

    # load details about the variable
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    # go through countries
    for iso in isos:
        print(iso)

        # again check whether the iso has to be treated
        path_out = '/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'
        file_out = path_out+'_'.join([iso,var])+'.nc'

        raw_files = sorted(glob.glob(path_out + iso + '/*regAv.nc'))
        if len(raw_files) > 10:
            # nc = xr.open_mfdataset(raw_files, concat_dim='wlvl')
            # nc = nc.assign_coords(wlvl=[fl.split('_')[-2] for fl in raw_files])
            # diff = nc['diff']

            datas = []
            for fl in raw_files:
                nc = xr.load_dataset(fl)
                tmp = nc['diff']
                if 'wlvl' not in nc.coords.keys():
                    tmp.coords['wlvl'] = fl.split('_')[-2]
                datas.append(tmp)

            diff = xr.concat(datas, dim='wlvl')
            diff.values[np.isfinite(diff.values) == False] = np.nan
            out_median = diff.median('ID')
            deviations = diff - out_median

            if os.path.isfile(file_out) == False or args['overwrite']:

                # prepare output arrays
                lower_bound = xr.DataArray(coords={'region':diff.region.values, 'wlvl':diff.wlvl.values, 'aggregation':diff.aggregation.values, 'season':diff.season.values}, dims=['wlvl','region','aggregation','season'])
                upper_bound = xr.DataArray(coords={'region':diff.region.values, 'wlvl':diff.wlvl.values, 'aggregation':diff.aggregation.values, 'season':diff.season.values}, dims=['wlvl','region','aggregation','season'])

                # for each region / mask style / season : 
                for region in diff.region.values:
                    for aggr in diff.aggregation.values:
                        for season in diff.season.values:
                            # select relevant deviations
                            devs_all = deviations.loc[:,region,aggr,season,:].copy()
                            # filter out gcm/im combinations that only exist in ref
                            devs_all = devs_all[:,np.isfinite(devs_all.loc['1.5'].values)]
                            # take only warming levels for which all gcm/im are available
                            devs = devs_all[np.isfinite(devs_all.values.mean(axis=1)),:]

                            if len(devs) != 0:
                                # create a dataFrame with a column for warming levels and one for deviations
                                data = pd.DataFrame()
                                data['wlvl'] = np.repeat(np.array(devs.wlvl.values, np.float), devs.shape[1], 0)
                                data['dev'] = devs.values.flatten()

                                # define quantile regression model
                                mod = smf.quantreg('dev ~ wlvl', data)
                                # for both upper and lower bound
                                for q,bound in zip([0.95,0.05],[upper_bound,lower_bound]):
                                    # fit quantile regression
                                    res = mod.fit(q=q)
                                    # get a line for the quantile regression for all warming levels
                                    slope = np.array(bound.wlvl.values, np.float) * res.params[1] + res.params[0]
                                    # check if this line crosses zero #or if the fit is horrible
                                    if (slope.min() < 0 and slope.max() > 0):# or res.pvalues.wlvl > 0.1:
                                        # if that is the case just take a constant quantile
                                        print('crossing',region,aggr,season, slope.min(), slope.max(), res.pvalues.wlvl)
                                        bound.loc[:,region,aggr,season] = np.nanpercentile(devs, q*100)
                                    else:
                                        # otherwise store the slope
                                        bound.loc[:,region,aggr,season] = np.array(bound.wlvl.values, np.float) * res.params[1] + res.params[0]

                                # create plots for some countries
                                if iso in args['plotCous'] and region == iso:
                                    os.system('mkdir -p /p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou/')
                                    # plot showing the quantile regression
                                    fig,ax = plt.subplots(nrows=1)
                                    ax.set_title(' '.join([region,aggr,season]))
                                    ax.scatter(data.wlvl,data.dev)

                                    x = np.array(bound.wlvl.values, np.float)
                                    for q,bound in zip([0.05,0.95],[upper_bound,lower_bound]):
                                        ax.plot(x, bound.loc[:,region,aggr,season])
                                    ax.axhline(y=0, color='k')
                                    ax.set_ylabel(details['long_name']+' - '+details['rel-abs'])
                                    ax.set_xlabel('wlvl')
                                    plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou/'+'_'.join([iso,var,region,aggr,season,])+'_deviations.png' , bbox_inches='tight')

                                    # plot showing the number of models and the atual result
                                    plt.close('all')
                                    fig,axes = plt.subplots(nrows=2, sharex=True, gridspec_kw={'height_ratios':[2,4]})

                                    x = np.array(out_median.wlvl,np.float)
                                    axes[0].set_title(' '.join([region,aggr,season]))
                                    axes[0].plot(x, np.isfinite(devs_all).sum('ID'))
                                    axes[0].set_ylabel('ensemble size')

                                    axes[1].plot(x, out_median.loc[:,region,aggr,season])
                                    axes[1].fill_between(x, out_median.loc[:,region,aggr,season]+lower_bound.loc[:,region,aggr,season], out_median.loc[:,region,aggr,season]+upper_bound.loc[:,region,aggr,season], alpha=0.5)
                                    axes[1].set_ylabel(details['long_name']+' - '+details['rel-abs'])
                                    plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou/'+'_'.join([iso,var,region,aggr,season,])+'_summary.png' , bbox_inches='tight')

                # save data
                # this is the file that is needed
                xr.Dataset({'median':out_median, 'lower_bound':lower_bound, 'upper_bound':upper_bound}).to_netcdf(file_out)


