
import numpy as np
import pandas as pd
import xarray as xr
import os

def return_rx5day_from_raw_file(file_path):
    dataset = xr.open_dataset(file_path)
    print(f'working on {file_path}')
    # Extract the precipitation variable (assuming it's named 'pr')
    precipitation = dataset['pr']

    # Define a rolling window of 5 days and calculate the sum
    rolling_sum = precipitation.rolling(time=5, min_periods=1).sum()

    # Calculate the yearly maximum of the rolling sum
    yearly_max = rolling_sum.groupby('time.year').max(dim='time')


    return yearly_max['year'].values.tolist() , yearly_max.values
    # Print the resulting yearly maximum values


def preprocess(model, scenario):

    example_file_path = f'/mnt/PROVIDE/niklas/extremerainfall/input/{model}/{scenario}/pr_day_{model}_{scenario}_r1i1p1_EWEMBI_20110101-20201231.nc4'
    example_file = xr.open_dataset(example_file_path)
    # Define the dimensions
    lon = example_file['lon'].values
    lat = example_file['lat'].values
    time = []
    # Create a random example data for rx5day (replace with your actual data)
    if os.path.exists(f'/mnt/PROVIDE/niklas/extremerainfall/input/{model}/{scenario}/pr_day_{model}_{scenario}_r1i1p1_EWEMBI_20910101-21001231.nc4'):
        end = 2100
        data = np.random.rand(120, len(lat), len(lon))
        relevant_years = ['20060101-20101231','20110101-20201231', '20210101-20301231', '20310101-20401231', '20410101-20501231', '20510101-20601231', '20610101-20701231', '20710101-20801231', '20810101-20901231', '20910101-21001231']
    elif os.path.exists(f'/mnt/PROVIDE/niklas/extremerainfall/input/{model}/{scenario}/pr_day_{model}_{scenario}_r1i1p1_EWEMBI_20910101-20991231.nc4'):
        end = 2099
        data = np.random.rand(119, len(lat), len(lon))
        relevant_years = ['20060101-20101231', '20110101-20201231', '20210101-20301231', '20310101-20401231',
                          '20410101-20501231', '20510101-20601231', '20610101-20701231', '20710101-20801231',
                          '20810101-20901231', '20910101-20991231']
    else:
        print('ERROR MAJOR MISTAKE IN CODE')

    for year in ['19810101-19901231','19910101-20001231','20010101-20051231']:
        file_path = f'/mnt/PROVIDE/niklas/extremerainfall/input/{model}/historical/pr_day_{model}_historical_r1i1p1_EWEMBI_{year}.nc4'
        add_time, add_data =  return_rx5day_from_raw_file(file_path)
        time_before = len(time)
        time.extend(add_time)
        data[time_before:len(time), :, :] = add_data
    for year in relevant_years:
        file_path = f'/mnt/PROVIDE/niklas/extremerainfall/input/{model}/{scenario}/pr_day_{model}_{scenario}_r1i1p1_EWEMBI_{year}.nc4'
        add_time, add_data = return_rx5day_from_raw_file(file_path)
        time_before = len(time)
        time.extend(add_time)
        data[time_before:len(time), :, :] = add_data

    # Create the xr.DataArray
    rx5day = xr.DataArray(data, coords={'lon': lon, 'lat': lat, 'time': time}, dims=['time', 'lat', 'lon'],  name='rx5day')

    output_file = f'/mnt/PROVIDE/niklas/extremerainfall/rawfiles/rx5day_{model}_{scenario}_1980_{end}.nc4'
    rx5day.to_netcdf(output_file, format="NETCDF4")
    print(f'finished {output_file}')

models = ['MIROC5']#['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']
scenarios = ['rcp26','rcp60','rcp85']
for model in models:
    for scenario in scenarios:
        preprocess(model, scenario)