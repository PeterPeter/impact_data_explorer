import os,sys, importlib, glob,gc
import numpy as np
import topojson as tp
import geopandas
import pandas as pd

import fiona
import cartopy.io.shapereader as shapereader 

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

isos = [fl.split('/')[-1] for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/*')]

def number_of_coords(shape):
	count = 0
	for sha in shape.geometry.values:
		count += len(sha.exterior.coords)
	return count

def drop_small_features(shape, N=300):
	shape = shape.reset_index()
	tmp_shape = shape.to_crs(epsg='6933')
	areas = np.array([tmp_shape.iloc[i]['geometry'].area / 10**6 for i in shape.index])
	return shape.iloc[np.argsort(areas)[::-1][:N]]

def simplify_to_number_of_points(shape,N=500):
	tol = 0.001
	if number_of_coords(shape) > N:
		while True:
			shape = shape.simplify(tol)
			tol *= 2
			print(tol, number_of_coords(shape))
			if number_of_coords(shape) < N:
				break
	
	return shape


for iso in isos:
	print(iso)
	out_file = '/p/tmp/pepflei/impact_data_explorer_data/topojsons/'+iso+'_0_topo.json'
	if os.path.isfile(out_file) == False or True:
		try:
			# COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
			# COU.download_shapefile()
			shape_file_0 = glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/*adm*/*0.shp')[0]
			shape_0 = geopandas.read_file(shape_file_0)
			shape_0 = shape_0[['GID_0','NAME_0','geometry']]
			shape_0 = shape_0.explode()
			shape_0 = drop_small_features(shape_0, N=300)
			print(shape_0)
			shape_0 = simplify_to_number_of_points(shape_0, N=10000)
			# shape_0.geometry.values[0].exterior.xy[0] = np.round(shape_0.geometry.values[0].exterior.xy[0],5)
			# shape_0.geometry.values[0].exterior.xy[1] = np.round(shape_0.geometry.values[0].exterior.xy[1],5)
			topo = tp.Topology(shape_0, prequantize=False).to_json()
			with open(out_file, 'w') as outfile:
				outfile.write(topo)
		except:
			pass

	out_file = '/p/tmp/pepflei/impact_data_explorer_data/topojsons/'+iso+'_1_topo.json'
	if os.path.isfile(out_file) == False or True:
		try:
			shape_file_1 = glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/*adm*/*1.shp')[0]
			shape_1 = geopandas.read_file(shape_file_1)[['HASC_1','NAME_1','geometry']]
			shape_1 = shape_1.explode()
			for tol in np.arange(0.005, 0.1, 0.005):
				if len(shape_1.simplify(tol).geometry.values[0].exterior.xy[0]) < 300:
					break
			shape_1.geometry = shape_1.simplify(tol)
			topo = tp.Topology(shape_1, prequantize=False).to_json()			
			with open(out_file, 'w') as outfile:
				outfile.write(topo)
		except:
			pass

	gc.collect()




#
