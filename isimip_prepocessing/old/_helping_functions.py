import os,sys, importlib, glob,gc

import numpy as np
import xarray as xr
import pandas as pd


'''
here are functions that are required by
- 1_glob_warming_level_difference.py
- 2_cou_data_1_regional_averages.py

I thought it would be good tho have this in one place to be sure that things are consistent
'''

def select_GCM_IM_combinations(IDs, excludeImpModels=[]):
    # prepare to average over rcps
    # IDs_gcm_im -> same length as IDs but with rcp information omitted
    # IDs_gcm_im_unique -> removed duplicates in IDs_gcm_im
    IDs_gcm_im = np.array(['_'.join([i for i in id_.split('_') if 'rcp' not in i]) for id_ in IDs])
    IDs_gcm_im_unique = np.unique(IDs_gcm_im)

    # ignore some impact models if they are incomplete for example
    # this has to be specified in the details of the variable under 'excludeImpModels' 
    if len(IDs_gcm_im_unique[0].split('_')) == 2:
        IDs_selected = [id_ for id_ in IDs_gcm_im_unique if id_.split('_')[1] not in excludeImpModels]
    else:
        IDs_selected = IDs_gcm_im_unique

    return IDs_selected, IDs_gcm_im

def average_over_rcps(diff_all, IDs_selected, IDs_gcm_im):
    gcm_im_combis = []
    for id_ in IDs_selected:
        gcm_im_combis.append(diff_all[IDs_gcm_im == id_].mean('ID'))
    diff = xr.concat(gcm_im_combis, dim='ID')
    diff = diff.assign_coords(ID=IDs_selected)
    return diff




