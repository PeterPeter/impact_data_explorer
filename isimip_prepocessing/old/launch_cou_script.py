import os,sys, importlib, glob,gc

import cartopy.io.shapereader as shapereader

cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()

isos = [info.attributes['adm0_a3'] for info in cous]

try:
	script = sys.argv[1]
except:
	script = 'cou_level/prep_cou_masks.py'

for iso in isos:
	print('sbatch job_single.sh '+script+' '+iso)
	os.system('sbatch job_single.sh '+script+' '+iso)




#
