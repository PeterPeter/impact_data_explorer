import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader
import statsmodels.formula.api as smf


import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

# load details about the variable
sys.path.append('global_time_slices')
exec("import %s; importlib.reload(%s); from %s import *" % tuple(['helping_functions']*3))

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-i','--isos', help='isos', nargs='+', default=[])
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['thawdepth'])

parser.add_argument('-e','--excludeImpModels', help='impact models to be excluded', nargs='+', default=[])

parser.add_argument('-p','--plotCous', help='list of countries for which to create plots', nargs='+', default=['DEU','BEN','CHN'])

parser.add_argument('--subregions', dest='subregions', action='store_true')
parser.add_argument('--no-subregions', dest='subregions', action='store_false')
parser.set_defaults(subregions=False)

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

# in case no iso was specified -> take all isos
if len(args['isos']) == 0:
	cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
	isos = [info.attributes['adm0_a3'] for info in cous]
else:
	isos = args['isos']

# wlvls from 1 - 5 ind 0.1 steps
wlvls = [str(w) for w in list(np.arange(1.0,5.0,0.1).round(1))]

# dict { season name : list(months) }
season_dict = {
	'annual' : list(range(1,13)),
	'DJF' : [1,2,12],
	'MAM' : [3,4,5],
	'JJA' : [6,7,8],
	'SON' : [9,10,11],
}

# this is goinf to be initialized with the first wlvl
iso_dict = {}

for var in args['vars']:
    print(var)

    # load details about the variable
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    # here I load reference slices
    # I load them several times - once for each RCP
    # I need this to be able to easily calculate changes later on
    files_all = glob.glob(details['wlvl_files_pattern'])
    files = [f for f in files_all if '1986-2006' in f]
    ref = []
    for rcp in ['rcp26','rcp45','rcp60','rcp85']:
        # IDs are all the information about a slice except the warming level (gcm, rcp, impModel)
        IDs = [f.split('/')[-1].replace('_'+'1986-2006'+'.nc','').replace('_'+var,'').replace('historical',rcp) for f in files]
        tmp = xr.open_mfdataset(files, concat_dim='ID')[var]#.sel({'lat':lats, 'lon':lons})
        tmp = tmp.assign_coords(ID=IDs)
        ref.append(tmp)
    ref = xr.concat(ref, dim='ID')

    # load the data into the RAM
    ref.load()

    for wlvl in wlvls:
        # this might free RAM during the loops
        gc.collect()
        print(wlvl)

        # this is just for me to check progress
        with open('progress/'+var+'_cou.txt', 'w') as fl_:
            fl_.write(wlvl+'\n')

        # here I load all files for the warming level
        files_all = glob.glob(details['wlvl_files_pattern'])
        files = [f for f in files_all if wlvl in f]
        IDs = [f.split('/')[-1].replace('_'+wlvl+'.nc','').replace('_'+var,'') for f in files]
        proj = xr.open_mfdataset(files, concat_dim='ID')[var]#.sel({'lat':lats, 'lon':lons})
        proj = proj.assign_coords(ID=IDs)
    
        # load into RAM
        proj.load()

        # here I make a selection of IDs that are available in ref and proj
        IDs = ref.ID.values[np.isin(ref.ID, proj.ID)]

        # this will be required later when averageing over RCPs
        IDs_selected, IDs_gcm_im = select_GCM_IM_combinations(IDs=IDs, excludeImpModels=args['excludeImpModels'])

        # go through countries - somehow I call them isos because I work with 3 letter country codes
        for iso in isos:
            if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*')) > 0:
                print(iso)

                # the following block only happens for the first warming level
                # it loads masks, prepares output arrays etc.
                if wlvl == wlvls[0]:
                    # load masks
                    COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
                    COU.load_mask()

                    # put masks in new dict
                    mask_dict = {
                        'area' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['latWeight'],
                        'pop' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['pop2005'],
                        'gdp' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['gdp2005'],
                    }
                    # I'll take lons and lats from this mask
                    mask_overlap = COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['overlap']

                    # depending on --subregions or --no-subregions the region names are loaded
                    if args['subregions']:
                        region_names = mask_overlap.coords['region'].values
                    else:
                        region_names = [iso]

                    # this is the output array for the median
                    out_median = xr.DataArray(coords={'region':region_names, 'wlvl':wlvls, 'aggregation':list(mask_dict.keys()), 'season':list(season_dict.keys())}, dims=['wlvl','region','aggregation','season'])

                    # in this array I will store deviations from the median
                    deviation = xr.DataArray(coords={'region':region_names, 'wlvl':wlvls, 'aggregation':list(mask_dict.keys()), 'season':list(season_dict.keys()),'ID':np.unique(IDs_gcm_im)}, dims=['wlvl','region','aggregation','season','ID'])

                    # all details are stored in the iso_dict
                    iso_dict[iso] = {
                        'lats' : mask_overlap.coords['lat'].values,
                        'lons' : mask_overlap.coords['lon'].values,
                        'median' : out_median,
                        'deviations' : deviation,
                        'mask_dict' : mask_dict,
                        'region_names' : region_names
                    }

                # get details for the iso
                iso_details = iso_dict[iso]

                # create output path it does not exist
                path_out = '/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'
                os.system('mkdir -p '+path_out)

                # define output file name and only continue if that file does not exist or the option is --overwrite
                file_out = path_out+'_'.join([iso,var])+'.nc'
                if os.path.isfile(file_out) == False or args['overwrite']:
                    for aggr,masks in iso_details['mask_dict'].items():
                        for region in iso_details['region_names']:

                            # select mask
                            mask = masks.loc[region]

                            # average over area
                            ref_area = (ref.sel(ID=IDs).sel({'lat':iso_details['lats'], 'lon':iso_details['lons']}) * mask).sum('lat').sum('lon')
                            proj_area = (proj.sel(ID=IDs).sel({'lat':iso_details['lats'], 'lon':iso_details['lons']}) * mask).sum('lat').sum('lon')

                            # the next block is a simplification for annual data -> check the "else" block for details
                            if details['temporal_resolution'] == 'yearly':
                                ref_area_seas = ref_area
                                proj_area_seas = proj_area

                                if details['rel-abs'] == 'relative':
                                    diff_all = (proj_area_seas - ref_area_seas) / ref_area_seas * 100
                                if details['rel-abs'] == 'absolute':
                                    diff_all = proj_area_seas - ref_area_seas

                                diff_all = diff_all.load()

                                # average over rcps
                                diff = average_over_rcps(diff_all=diff_all, IDs_selected=IDs_selected, IDs_gcm_im=IDs_gcm_im)

                                iso_details['median'].loc[wlvl,region,aggr,'annual'] = diff.median()
                                iso_details['deviations'].loc[wlvl,region,aggr,'annual',diff.ID] = diff - diff.median()

                            else:
                                for season,months in season_dict.items():
                                    # select months of season and aggregate depending on the "seasonal_aggregation" in details
                                    ref_area_seas = ref_area.loc[:,months].reduce(details['seasonal_aggreagation'], 'month')
                                    proj_area_seas = proj_area.loc[:,months].reduce(details['seasonal_aggreagation'], 'month')

                                    # this is now the change in the the variable for a warming level for all different gcm-impModel-rcp combinations
                                    if details['rel-abs'] == 'relative':
                                        diff_all = (proj_area_seas - ref_area_seas) / ref_area_seas * 100
                                    if details['rel-abs'] == 'absolute':
                                        diff_all = proj_area_seas - ref_area_seas

                                    # load into RAM if it isn't already there
                                    diff_all = diff_all.load()

                                    # average over rcps
                                    diff = average_over_rcps(diff_all=diff_all, IDs_selected=IDs_selected, IDs_gcm_im=IDs_gcm_im)

                                    # store median and deviations from median
                                    iso_details['median'].loc[wlvl,region,aggr,season] = diff.median()
                                    iso_details['deviations'].loc[wlvl,region,aggr,season,diff.ID] = diff - diff.median()

    # save data
    for iso, iso_details in iso_dict.items():
        print('writing '+iso)
        path_out = '/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'
        os.system('mkdir -p '+path_out)
        file_out = path_out+'_'.join([iso,var])+'.nc'
        # this is an in between result which allows to skip the first large block of this script
        xr.Dataset({'deviation':iso_details['deviations']}).to_netcdf(file_out.replace('.nc','_deviations.nc'))



    # this comes when all warming levels have been done
    for iso, iso_details in iso_dict.items():
        print('writing '+iso)

        # again check whether the iso has to be treated
        path_out = '/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'
        os.system('mkdir -p '+path_out)
        file_out = path_out+'_'.join([iso,var])+'.nc'
        if os.path.isfile(file_out) == False or args['overwrite']:

            # prepare output arrays
            lower_bound = xr.DataArray(coords={'region':iso_details['region_names'], 'wlvl':wlvls, 'aggregation':list(iso_details['mask_dict'].keys()), 'season':list(season_dict.keys())}, dims=['wlvl','region','aggregation','season'])
            upper_bound = xr.DataArray(coords={'region':iso_details['region_names'], 'wlvl':wlvls, 'aggregation':list(iso_details['mask_dict'].keys()), 'season':list(season_dict.keys())}, dims=['wlvl','region','aggregation','season'])

            # for each region / mask style / season : 
            for region in iso_details['region_names']:
                for aggr,masks in iso_details['mask_dict'].items():
                    for season,months in season_dict.items():
                        # select relevant deviations
                        devs = iso_details['deviations'].loc[:,region,aggr,season,:]
                        # filter out gcm/im combinations that only exist in ref
                        devs = devs[:,np.isfinite(devs.loc['1.5'].values)]
                        # take only warming levels for which all gcm/im are available
                        devs = devs[np.isfinite(devs.values.mean(axis=1)),:]

                        if len(devs) != 0:
                            # create a dataFrame with a column for warming levels and one for deviations
                            data = pd.DataFrame()
                            data['wlvl'] = np.repeat(np.array(devs.wlvl.values, np.float), devs.shape[1], 0)
                            data['dev'] = devs.values.flatten()

                            # define quantile regression model
                            mod = smf.quantreg('dev ~ wlvl', data)
                            # for both upper and lower bound
                            for q,bound in zip([0.95,0.05],[upper_bound,lower_bound]):
                                # fit quantile regression
                                res = mod.fit(q=q)
                                # store the quantile regression slope
                                bound.loc[:,region,aggr,season] = np.array(bound.wlvl.values, np.float) * res.params[1] + res.params[0]

            # save data
            # this is the file that is needed
            xr.Dataset({'median':iso_details['median'], 'lower_bound':lower_bound, 'upper_bound':upper_bound}).to_netcdf(file_out)
            # this is an in between result which allows to skip the first large block of this script
            xr.Dataset({'deviation':iso_details['deviations']}).to_netcdf(file_out.replace('.nc','_deviations.nc'))

            # create plots for some countries
            if iso in args['plotCous']:
                path_out = '/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou/'
                os.system('mkdir -p '+path_out)

                with PdfPages(path_out+'_'.join([iso,var])+'_deviations.pdf') as pdf:
                    for region in iso_details['region_names']:
                        for aggr,masks in iso_details['mask_dict'].items():
                            for season,months in season_dict.items():
                                devs = iso_details['deviations'].loc[:,region,aggr,season,:]
                                # filter out gcm/im combinations that only exist in ref
                                devs = devs[:,np.isfinite(devs.loc['1.5'].values)]
                                # take only warming levels for which all gcm/im are available
                                devs = devs[np.isfinite(devs.values.mean(axis=1)),:]
                                if len(devs) != 0:
                                    data = pd.DataFrame()
                                    data['wlvl'] = np.repeat(np.array(devs.wlvl.values, np.float), devs.shape[1], 0)
                                    data['dev'] = devs.values.flatten()
                                    plt.close('all')
                                    fig,ax = plt.subplots(nrows=1)
                                    ax.set_title(' '.join([region,aggr,season]))
                                    ax.scatter(data.wlvl,data.dev)

                                    mod = smf.quantreg('dev ~ wlvl', data)
                                    x = np.array(bound.wlvl.values, np.float)
                                    for q,bound in zip([0.05,0.95],[upper_bound,lower_bound]):
                                        res = mod.fit(q=q)
                                        ax.plot(x, x * res.params[1] + res.params[0])

                                    ax.axhline(y=0, color='k')
                                    ax.set_ylabel(details['long_name']+' - '+details['rel-abs'])
                                    ax.set_xlabel('wlvl')
                                    pdf.savefig(bbox_inches='tight')

                with PdfPages(path_out+'_'.join([iso,var])+'.pdf') as pdf:
                    for region in iso_details['region_names']:
                        for aggr,masks in iso_details['mask_dict'].items():
                            for season,months in season_dict.items():
                                plt.close('all')
                                fig,axes = plt.subplots(nrows=2, sharex=True, gridspec_kw={'height_ratios':[2,4]})
                                x = np.array(out_median.wlvl,np.float)

                                devs = iso_details['deviations'].loc[:,region,aggr,season,:]
                                # filter out gcm/im combinations that only exist in ref
                                devs = devs[:,np.isfinite(devs.loc['1.5'].values)]

                                axes[0].set_title(' '.join([region,aggr,season]))
                                axes[0].plot(x, np.isfinite(devs).sum('ID'))
                                axes[0].set_ylabel('ensemble size')

                                axes[1].plot(x, iso_details['median'].loc[:,region,aggr,season])
                                axes[1].fill_between(x, iso_details['median'].loc[:,region,aggr,season]+lower_bound.loc[:,region,aggr,season], iso_details['median'].loc[:,region,aggr,season]+upper_bound.loc[:,region,aggr,season], alpha=0.5)
                                axes[1].set_ylabel(details['long_name']+' - '+details['rel-abs'])
                                pdf.savefig(bbox_inches='tight')




#
