import os,sys, importlib, glob,gc,pickle
import numpy as np
import topojson as tp
import geopandas
import pandas as pd

import fiona
import cartopy.io.shapereader as shapereader 

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

isos = sorted([fl.split('/')[-1] for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/*')])


for iso in isos:
    print(iso)
    files = glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/*adm*/*1.shp')
    if len(files) > 0:
        shape_file_1 = files[0]
        shape_1 = geopandas.read_file(shape_file_1, encoding="utf-8")[['HASC_1','NAME_1']]
        region_names = {i:n for i,n in zip(shape_1.HASC_1, shape_1.NAME_1)}

        pickle.dump(region_names, open('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/region_names.pkl', 'wb'))
        



#
