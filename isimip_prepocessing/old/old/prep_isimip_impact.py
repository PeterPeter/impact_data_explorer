
import os,sys,glob,gc,importlib

import numpy as np
import xarray as xr
import pandas as pd

'''
for gcm in GFDL-ESM2M HadGEM2-ES IPSL-CM5A-LR MIROC5; do for scenario in rcp26 rcp45 rcp60 rcp85 historical; do sbatch job_single.sh global_time_slices/prep_isimip_impact.py maxdis $gcm $scenario run; done; done;
'''

os.chdir('/home/pepflei/Projects/impact_data_explorer/')

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-v','--var', help='variable', required=True)
parser.add_argument('-g','--gcm', help='gcm', required=False, default=None)
parser.add_argument('-s','--scenario', help='scenario', required=False, default=None)
parser.add_argument('-o','--overwrite', help='overwrite existing files?', required=False, default=True)
parser.add_argument('-m','--mode', help='only test?', required=False, default='run')
parser.add_argument('-w','--wlvls', help='warming levels', nargs='+', default=[])
args = vars(parser.parse_args())

short_name = args['var']

if args['gcm'] is not None:
    gcms = [args['gcm']]
else:
    gcms = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']

if args['scenario'] is not None:
    scenarios = [args['scenario']]
else:
    scenarios = ['historical','rcp26','rcp45','rcp60','rcp85']

overwrite = args['overwrite']
mode = args['mode']
selected_wlvls = args['wlvls']

'''
short_name = 'rsdsAdjust'
gcms = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']
gcms = ['HadGEM2-ES']
scenarios = ['rcp26']
mode = 'test'
overwrite = True
'''

sys.path.append('meta/variables/')
exec("import %s; importlib.reload(%s); from %s import *" % tuple([short_name+'_check']*3))

if 'in_files' in details.keys():
    files_all = details['in_files']
else:
    files_all = glob.glob(details['in_files_pattern_historical']) + glob.glob(details['in_files_pattern_future'])

wlvlYears_csv = pd.read_csv('meta/warming_lvls_cmip5_21_years.csv')

impModels = np.unique([ff.split('/')[-4] for ff in files_all])

wlvls = np.array(wlvlYears_csv.wlvl.round(1), str)
wlvlYears = xr.DataArray(coords={'gcm':gcms, 'scenario':scenarios, 'wlvl':['1986-2006']+list(wlvls)}, dims=['gcm','scenario','wlvl'])
for gcm in gcms:
    if 'historical' in scenarios:
        wlvlYears.loc[gcm,'historical','1986-2006'] = 1996
    for scenario in [scen for scen in scenarios if scen!='historical']:
        for wlvl in wlvls:
            wlvlYears.loc[gcm,scenario,wlvls] = wlvlYears_csv[gcm+'_'+scenario]

if len(selected_wlvls) == 0:
    selected_wlvls = wlvlYears.wlvl.values

print(short_name,gcms,scenarios,overwrite,mode,selected_wlvls)

for gcm in gcms:
    for scenario in scenarios:
        for wlvl in selected_wlvls:
            if np.isfinite(wlvlYears.loc[gcm,scenario,wlvl]):
                years = np.arange(wlvlYears.loc[gcm,scenario,wlvl]-10,wlvlYears.loc[gcm,scenario,wlvl]+11,1)
                for impModel in impModels:

                    outPath = '/p/tmp/pepflei/impact_data_explorer_data/raw/'+'/'.join([short_name,gcm,scenario,impModel])+'/'
                    outFile = outPath+'_'.join([gcm,scenario,impModel,short_name,wlvl])+'.nc'
                    if os.path.isfile(outFile) == False or details['overwrite']=='overwrite':

                        files_sel = [ff for ff in files_all if ff.split('/')[-3] == gcm.lower()]
                        files_sel = [ff for ff in files_sel if ff.split('/')[-4] == impModel]

                        files_scen = [ff for ff in files_sel if ff.split('/')[-1].split('_')[3] == scenario]

                        if len(files_scen) > 0:
                            required_files = []

                            if years.min() < 2006 and years.max() > 2006:
                                print('requiring hist years')
                                files_hist = [ff for ff in files_sel if ff.split('/')[-1].split('_')[3] == 'historical']

                                files_scen += files_hist

                            for file_name in files_scen:
                                start,stop = (float(fl) for fl in file_name.split('.')[0].split('_')[-2:])
                                if np.sum(np.isin(years,np.arange(start,stop+1,1))) > 0:
                                    required_files.append(file_name)

                            if mode == 'test':
                                print(required_files)

                            if details['temporal_resolution'] == 'daily':
                                data = xr.open_mfdataset(required_files, combine='by_coords', concat_dim='time')[details['orig_name']]
                                monthly = data.resample({'time':'1M'}).reduce(details['monthly_aggreagation'])
                                wlvlSlice = monthly.loc[np.isin(monthly.time.dt.year,years),:,:].groupby('time.month').mean('time')

                            if details['temporal_resolution'] == 'monthly':
                                data = xr.open_mfdataset(required_files, combine='by_coords', concat_dim='time', decode_times=False)
                                data.time.attrs['calendar'] = '360_day'
                                monthly = xr.decode_cf(data, decode_times=True)[details['orig_name']]
                                wlvlSlice = monthly.loc[np.isin(monthly.time.dt.year,years),:,:].groupby('time.month').mean('time')

                            if details['temporal_resolution'] == 'yearly':
                                data = xr.open_mfdataset(required_files, combine='by_coords', concat_dim='time', decode_times=False)
                                time_units = data.time.attrs['units']
                                ref_year = time_units.split('years since ')[-1].split('-')[0]
                                data = data[details['orig_name']].assign_coords(time=data.time.values+int(ref_year))
                                if len(ref_year) == 4:
                                    wlvlSlice = data[np.isin(data.time.values,years),:,:].mean('time')

                            if os.path.isdir(outPath) == False:
                                os.system('mkdir -p '+outPath)
                            print('writing: '+outFile)
                            xr.Dataset({details['short_name']:wlvlSlice}).to_netcdf(outFile)

                            gc.collect()
                            if mode == 'test':
                                print('working ' + outFile)
                                asdas

                    else:
                        print('allready done ' + outFile)

# file_object = open('progress_raw.txt', 'a')
# file_object.write(short_name+'\n')
# file_object.close()





#
