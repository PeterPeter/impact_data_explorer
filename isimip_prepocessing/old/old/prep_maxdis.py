
import os,sys,glob,gc

import numpy as np
import xarray as xr
import pandas as pd

'''
for gcm in GFDL-ESM2M HadGEM2-ES IPSL-CM5A-LR MIROC5; do for scenario in rcp26 rcp45 rcp60 rcp85 historical; do sbatch job_single.sh prepare_data_global_maxdis.py $gcm $scenario; done; done;
'''


os.chdir('/home/pepflei/Projects/impact_data_explorer/')

try:
    gcms = [sys.argv[1]]
    scenarios = [sys.argv[2]]
except:
    gcms = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']
    scenarios = ['rcp26','rcp45','rcp60','rcp85','historical']

wlvlYears_csv = pd.read_csv('warming_lvls_cmip5_21_years.csv')

impModels = np.unique([ff.split('/')[-1] for ff in glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*')])

wlvls = np.array(wlvlYears_csv.wlvl.round(1), str)
wlvlYears = xr.DataArray(coords={'gcm':gcms, 'scenario':scenarios, 'wlvl':['1986-2006']+list(wlvls)}, dims=['gcm','scenario','wlvl'])
for gcm in gcms:
    if 'historical' in scenarios:
        wlvlYears.loc[gcm,'historical','1986-2006'] = 1996
    for scenario in [scen for scen in scenarios if scen!='historical']:
        for wlvl in wlvls:
            wlvlYears.loc[gcm,scenario,wlvls] = wlvlYears_csv[gcm+'_'+scenario]

for gcm in gcms:
    for scenario in scenarios:
        for wlvl in wlvlYears.wlvl.values:
            if np.isfinite(wlvlYears.loc[gcm,scenario,wlvl]):
                years = np.arange(wlvlYears.loc[gcm,scenario,wlvl]-10,wlvlYears.loc[gcm,scenario,wlvl]+11,1)
                for impModel in impModels:
                    variable = 'dis'

                    outPath = '/p/tmp/pepflei/impact_data_explorer_data/'+'/'.join(['maxdis',gcm,scenario,impModel])+'/'
                    outFile = outPath+'_'.join([gcm,scenario,impModel,'maxdis',wlvl])+'.nc'
                    if os.path.isfile(outFile) == False or True:

                        if scenario == 'historical':
                            all_files = glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/'+impModel+'/'+gcm.lower()+'/*/*'+scenario+'*_histsoc_co2_'+variable+'_global_daily*')
                        else:
                            all_files = glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/'+impModel+'/'+gcm.lower()+'/*/*'+scenario+'*_2005soc_co2_'+variable+'_global_daily*')
                        if len(all_files) > 0:
                            required_files = []

                            if years.min() < 2006 and years.max() > 2006:
                                print('requiring hist years')
                                all_files += glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/'+impModel+'/'+gcm.lower()+'/*/*historical*_histsoc_co2_'+variable+'_global_daily*')

                            for file_name in all_files:
                                start,stop = (float(fl) for fl in file_name.split('.')[0].split('_')[-2:])
                                if np.sum(np.isin(years,np.arange(start,stop+1,1))) > 0:
                                    required_files.append(file_name)

                            data = xr.open_mfdataset(required_files, combine='by_coords', concat_dim='time')[variable]
                            monthly = data.resample({'time':'1M'}).reduce(np.mean)
                            wlvlSlice = monthly.loc[np.isin(monthly.time.dt.year,years),:,:].groupby('time.month').mean('time')

                            if os.path.isdir(outPath) == False:
                                os.system('mkdir -p '+outPath)
                            print('writing: '+outFile)
                            xr.Dataset({variable:wlvlSlice}).to_netcdf(outFile)

                            gc.collect()
                            asdasd

                        else:
                            pass







#
