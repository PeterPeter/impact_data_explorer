import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd

# some functions that are also used by 2_cou_data_1_regional_averages.py.py
sys.path.append('isimip_prepocessing')
exec("import %s; importlib.reload(%s); from %s import *" % tuple(['_helping_functions']*3))

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['ped'])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

season_dict = {
    'annual' : list(range(1,13)),
    'DJF' : [1,2,12],
    'MAM' : [3,4,5],
    'JJA' : [6,7,8],
    'SON' : [9,10,11],
}


wlvls_from_rcp_comparison = np.unique(list(pd.read_csv('meta/warming_levels_for_compare_function.csv').iloc[:,2:].values.flatten()))
wlvls_fixed = np.arange(1.5,3.5,0.5)

wlvls = np.unique(np.concatenate((wlvls_from_rcp_comparison, wlvls_fixed)))
wlvls = wlvls[wlvls<3.5]

wlvl_dict = {i+1:str(w) for i,w in enumerate(wlvls)}
wlvl_dict[0] = '1986-2006'



def get_difference(tmp_proj,tmp_ref, IDs_selected):
    '''
    this is a function that computes the differences and model agreement
    '''

    # this is now the change in the the var for a warming level for all different gcm-impModel-rcp combinations
    if details['rel-abs'] == 'relative':
        diff_all = ( tmp_proj.sel(ID=IDs) - tmp_ref.sel(ID=IDs) ) / tmp_ref.sel(ID=IDs) * 100
    if details['rel-abs'] == 'absolute':
        diff_all = tmp_proj.sel(ID=IDs) - tmp_ref.sel(ID=IDs)

    # average over rcps
    proj_ = average_over_rcps(diff_all=tmp_proj.sel(ID=IDs), IDs_selected=IDs_selected, IDs_gcm_im=IDs_gcm_im)
    ref_ = average_over_rcps(diff_all=tmp_ref.sel(ID=IDs), IDs_selected=IDs_selected, IDs_gcm_im=IDs_gcm_im)

    diff = proj_ - ref_

    # get the ensemble mean
    ens_mean = proj_.mean('ID') - ref_.mean('ID')

    # model agreement
    model_agreement = ens_mean.copy() * 0.0
    for ID in diff.ID.values:
        model_agreement += np.sign(diff.loc[ID]) == np.sign(ens_mean)
    model_agreement /= float(len(IDs_selected))

    return ens_mean, model_agreement


for var in args['vars']:
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))


    path_out = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/'
    os.system('mkdir -p '+path_out)

    for proj_wlvl_id in sorted(wlvl_dict.keys()):
        proj_wlvl = wlvl_dict[proj_wlvl_id]
        # here I load projection slices
        files_all = glob.glob(details['wlvl_files_pattern'])
        files = [f for f in files_all if proj_wlvl in f]
        IDs = [f.split('/')[-1].replace('_'+proj_wlvl+'.nc','').replace('_'+var,'') for f in files]
        proj = xr.open_mfdataset(files, concat_dim='ID')[var]
        proj = proj.assign_coords(ID=IDs)

        for ref_wlvl_id in range(proj_wlvl_id):
            ref_wlvl = wlvl_dict[ref_wlvl_id]
            print(proj_wlvl, ref_wlvl)

            if ref_wlvl == '1986-2006':
                # here I load all files for the warming level
                # I load them several times - once for each RCP
                # I need this to be able to easily calculate changes later on
                files_all = glob.glob(details['wlvl_files_pattern'])
                files = [f for f in files_all if '1986-2006' in f]
                ref = []
                for rcp in ['rcp26','rcp45','rcp60','rcp85']:
                    # IDs are all the information about a slice except the warming level (gcm, rcp, impModel)
                    IDs = [f.split('/')[-1].replace('_'+'1986-2006'+'.nc','').replace('_'+var,'').replace('historical',rcp) for f in files]
                    tmp = xr.open_mfdataset(files, concat_dim='ID')[var]
                    tmp = tmp.assign_coords(ID=IDs)
                    ref.append(tmp)
                ref = xr.concat(ref, dim='ID')
            else:
                # here I load all files for the warming level
                files_all = glob.glob(details['wlvl_files_pattern'])
                files = [f for f in files_all if ref_wlvl in f]
                IDs = [f.split('/')[-1].replace('_'+ref_wlvl+'.nc','').replace('_'+var,'') for f in files]
                ref = xr.open_mfdataset(files, concat_dim='ID')[var]
                ref = ref.assign_coords(ID=IDs)

            # here I make a selection of IDs that are available in ref and proj
            IDs = ref.ID.values[np.isin(ref.ID, proj.ID)]

            # this will be required later when averageing over RCPs
            if 'excludeImpModels' not in details.keys():
                details['excludeImpModels'] = []
            IDs_selected, IDs_gcm_im = select_GCM_IM_combinations(IDs=IDs, excludeImpModels=details['excludeImpModels'])

            # MAPS
            if details['temporal_resolution'] == 'yearly':
                # here for annual data
                out_file = path_out+'_'.join([var,proj_wlvl,ref_wlvl,'annual'])+'.nc'
                if os.path.isfile(out_file) == False or args['overwrite']:
                    tmp_proj,tmp_ref = proj,ref
                    ens_mean, model_agreement = get_difference(tmp_proj=tmp_proj, tmp_ref=tmp_ref, IDs_selected=IDs_selected)


                    asdas
                    ds = xr.Dataset({'var':ens_mean, 'model_agreement':model_agreement})
                    ds.attrs = {'GCM-IM input':IDs_selected, 'GCM-IM-RCP available':list(IDs)}
                    ds.to_netcdf(out_file)
                    gc.collect()

            else:
                # there is one map per seasonal aggreagtion
                for season,months in season_dict.items():
                    out_file = path_out+'_'.join([var,proj_wlvl,ref_wlvl,season])+'.nc'
                    if os.path.isfile(out_file) == False or args['overwrite']:
                        tmp_ref = ref.sel(month=months).reduce(details['seasonal_aggreagation'], 'month')
                        tmp_proj = proj.sel(month=months).reduce(details['seasonal_aggreagation'], 'month')

                        ens_mean, model_agreement = get_difference(tmp_proj=tmp_proj, tmp_ref=tmp_ref, IDs_selected=IDs_selected)
                        ds = xr.Dataset({'var':ens_mean, 'model_agreement':model_agreement})
                        ds.attrs = {'GCM-IM input':IDs_selected, 'GCM-IM-RCP available':list(IDs)}
                        ds.to_netcdf(out_file)

                        gc.collect()


