import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader
import statsmodels.formula.api as smf


# some functions that are also used by 1_glob_warming_level_difference.py
sys.path.append('isimip_prepocessing')
exec("import %s; importlib.reload(%s); from %s import *" % tuple(['_helping_functions']*3))

cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
isos = [info.attributes['adm0_a3'] for info in cous]
varis = [fl.split('/')[-1] for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/cou_data/*')]

for var in varis:
    print(var)

    for iso in isos:

        path_out = '/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'
        file_out = path_out+'_'.join([iso,var])+'.nc'

        raw_files = sorted(glob.glob(path_out + iso + '/*raw.nc'))
        for fl in raw_files:
            data = xr.load_dataset(fl)['diff']
            true_wlvl = fl.split('_')[-2]
            xr.Dataset({'diff':data.sel(wlvl=true_wlvl)}).to_netcdf(fl.replace('raw','regAv'))










