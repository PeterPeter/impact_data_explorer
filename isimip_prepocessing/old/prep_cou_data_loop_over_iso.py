import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader
import statsmodels.formula.api as smf


import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i','--iso', help='iso', required=False, default=None)
parser.add_argument('-v','--var', help='variable', required=False, default=None)

parser.add_argument('-e','--excludeImpModels', help='impact models to be excluded', nargs='+', default=[])

parser.add_argument('-p','--plotCous', help='list of countries for which to create plots', nargs='+', default=['DEU','BEN','CHN'])

parser.add_argument('--subregions', dest='subregions', action='store_true')
parser.add_argument('--no-subregions', dest='subregions', action='store_false')
parser.set_defaults(subregions=False)

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())

print(args)

if args['iso'] is None:
	cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
	isos = [info.attributes['adm0_a3'] for info in cous]
	#isos = ['USA']
else:
	isos = [args['iso']]


if args['var'] is None:
	# variables = [fl.split('/')[-1] for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/raw/*')]
	variables = ['tasmaxAdjust']
else:
	variables = [args['var']]

overwrite = args['overwrite']
subregions = args['subregions']

print(isos,variables,overwrite,subregions)

wlvls = [str(w) for w in list(np.arange(1.0,5.0,0.1).round(1))]

season_dict = {
	'annual' : list(range(1,13)),
	# 'q1' : list(range(1,4)),
	# 'q2' : list(range(4,7)),
	# 'q3' : list(range(7,10)),
	# 'q4' : list(range(10,13)),
	'DJF' : [1,2,12],
	'MAM' : [3,4,5],
	'JJA' : [6,7,8],
	'SON' : [9,10,11],
}

for iso in isos:
	print(iso)
	if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*')) > 0:
		COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
		COU.load_mask()

		mask_dict = {
			'area' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['latWeight'],
			'pop' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['pop2005'],
			'gdp' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['gdp2005'],
		}
		mask_overlap = COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['overlap']

		if subregions:
			region_names = mask_overlap.coords['region'].values
		else:
			region_names = [iso]
			
		lats = mask_overlap.coords['lat'].values
		lons = mask_overlap.coords['lon'].values

		for var in variables:
			print(var)

			sys.path.append('meta/variables/')
			exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

			path_out = '/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'
			os.system('mkdir -p '+path_out)
			file_out = path_out+'_'.join([iso,var])+'.nc'
			if os.path.isfile(file_out) == False or overwrite:
				ensemble = {}

				# here I load reference slices
				# I load them several times - once for each RCP
				# I need this to be able to easily calculate changes later on
				files_all = glob.glob(details['wlvl_files_pattern'])
				files = [f for f in files_all if '1986-2006' in f]
				ref = []
				for rcp in ['rcp26','rcp45','rcp60','rcp85']:
					# IDs are all the information about a slice except the warming level (gcm, rcp, impModel)
					IDs = [f.split('/')[-1].replace('_'+'1986-2006'+'.nc','').replace('_'+var,'').replace('historical',rcp) for f in files]
					tmp = xr.open_mfdataset(files, concat_dim='ID')[var].sel({'lat':lats, 'lon':lons})
					tmp = tmp.assign_coords(ID=IDs)
					ref.append(tmp)
				ref = xr.concat(ref, dim='ID')

				out_median = xr.DataArray(coords={'region':region_names, 'wlvl':wlvls, 'aggregation':list(mask_dict.keys()), 'season':list(season_dict.keys())}, dims=['region','wlvl','aggregation','season'])

				IDs_gcm_im = np.array(['_'.join([i for i in id_.split('_') if 'rcp' not in i]) for id_ in ref.ID.values])
				deviation = xr.DataArray(coords={'region':region_names, 'wlvl':wlvls, 'aggregation':list(mask_dict.keys()), 'season':list(season_dict.keys()), 'ID':np.unique(IDs_gcm_im)}, dims=['region','wlvl','aggregation','season','ID'])


				for wlvl in wlvls:
					print(wlvl)
					# here I load all files for the warming level
					files_all = glob.glob(details['wlvl_files_pattern'])
					files = [f for f in files_all if wlvl in f]
					IDs = [f.split('/')[-1].replace('_'+wlvl+'.nc','').replace('_'+var,'') for f in files]
					proj = xr.open_mfdataset(files, concat_dim='ID')[var].sel({'lat':lats, 'lon':lons})
					proj = proj.assign_coords(ID=IDs)

					# here I make a selection of IDs that are available in ref and proj
					IDs = ref.ID.values[np.isin(ref.ID, proj.ID)]

					for aggr,masks in mask_dict.items():
						for region in region_names:
							mask = masks.loc[region]

							ref_area = (ref.sel(ID=IDs) * mask).sum('lat').sum('lon')
							proj_area = (proj.sel(ID=IDs) * mask).sum('lat').sum('lon')

							for season,months in season_dict.items():
								ref_area_seas = ref_area.loc[:,months].reduce(details['seasonal_aggreagation'], 'month')
								proj_area_seas = proj_area.loc[:,months].reduce(details['seasonal_aggreagation'], 'month')

								# this is now the change in the the variable for a warming level for all different gcm-impModel-rcp combinations
								if details['rel-abs'] == 'relative':
									diff_all = (proj_area_seas - ref_area_seas) / ref_area_seas * 100
								if details['rel-abs'] == 'absolute':
									diff_all = proj_area_seas - ref_area_seas

								diff_all = diff_all.load()

								# prepare to average over rcps
								IDs_gcm_im = np.array(['_'.join([i for i in id_.split('_') if 'rcp' not in i]) for id_ in diff_all.ID.values])
								IDs_gcm_im_unique = np.unique(IDs_gcm_im)

								# # filter out incomplete gcm-im combinations if ims have not been run for all gcms
								# if len(IDs_gcm_im_unique[0].split('_')) == 2:
								# 	IDs_selected = []
								# 	for id_ in IDs_gcm_im_unique:	
								# 		im_ = id_.split('_')[1]
								# 		if len(np.unique([id_ for id_ in IDs_gcm_im if im_ in id_])) == 4:
								# 			IDs_selected.append(id_)
								# else:
								# 	IDs_selected = IDs_gcm_im_unique

								# ignore some impact models if they are incomplete for example
								if len(IDs_gcm_im_unique[0].split('_')) == 2:
									IDs_selected = [id_ for id_ in IDs_gcm_im_unique if id_.split('_')[1] not in args['excludeImpModels']]
								else:
									IDs_selected = IDs_gcm_im_unique

								# average over rcps
								gcm_im_combis = []
								for id_ in IDs_selected:
									gcm_im_combis.append(diff_all[IDs_gcm_im == id_].mean('ID'))
								diff = xr.concat(gcm_im_combis, dim='ID')
								diff = diff.assign_coords(ID=IDs_selected)

								out_median.loc[region,wlvl,aggr,season] = diff.median()
								deviation.loc[region,wlvl,aggr,season,diff.ID] = diff - diff.median()


				lower_bound = xr.DataArray(coords={'region':region_names, 'wlvl':wlvls, 'aggregation':list(mask_dict.keys()), 'season':list(season_dict.keys())}, dims=['region','wlvl','aggregation','season'])
				upper_bound = xr.DataArray(coords={'region':region_names, 'wlvl':wlvls, 'aggregation':list(mask_dict.keys()), 'season':list(season_dict.keys())}, dims=['region','wlvl','aggregation','season'])
				for region in region_names:
					for aggr,masks in mask_dict.items():
						for season,months in season_dict.items():
							devs = deviation.loc[region,:,aggr,season,:]
							# filter out gcm/im combinations that only exist in ref
							devs = devs[:,np.isfinite(devs.loc['1.5'].values)]
							# take only warming levels for which all gcm/im are available
							devs = devs[np.isfinite(devs.values.mean(axis=1)),:]
							if len(devs) != 0:
								data = pd.DataFrame()
								data['wlvl'] = np.repeat(np.array(devs.wlvl.values, np.float), devs.shape[1], 0)
								data['dev'] = devs.values.flatten()

								mod = smf.quantreg('dev ~ wlvl', data)
								for q,bound in zip([0.95,0.05],[upper_bound,lower_bound]):
									res = mod.fit(q=q)
									bound.loc[region,:,aggr,season] = np.array(bound.wlvl.values, np.float) * res.params[1] + res.params[0]


				xr.Dataset({'median':out_median, 'lower_bound':lower_bound, 'upper_bound':upper_bound}).to_netcdf(file_out)

				if iso in args['plotCous']:
					path_out = '/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou/'
					os.system('mkdir -p '+path_out)

					with PdfPages(path_out+'_'.join([iso,var])+'_deviations.pdf') as pdf:
						for region in region_names:
							for aggr,masks in mask_dict.items():
								for season,months in season_dict.items():
									devs = deviation.loc[region,:,aggr,season,:]
									# filter out gcm/im combinations that only exist in ref
									devs = devs[:,np.isfinite(devs.loc['1.5'].values)]
									# take only warming levels for which all gcm/im are available
									devs = devs[np.isfinite(devs.values.mean(axis=1)),:]
									if len(devs) != 0:
										data = pd.DataFrame()
										data['wlvl'] = np.repeat(np.array(devs.wlvl.values, np.float), devs.shape[1], 0)
										data['dev'] = devs.values.flatten()
										plt.close('all')
										fig,ax = plt.subplots(nrows=1)
										ax.set_title(' '.join([region,aggr,season]))
										ax.scatter(data.wlvl,data.dev)

										mod = smf.quantreg('dev ~ wlvl', data)
										x = np.array(bound.wlvl.values, np.float)
										for q,bound in zip([0.05,0.95],[upper_bound,lower_bound]):
											res = mod.fit(q=q)
											ax.plot(x, x * res.params[1] + res.params[0])

										ax.axhline(y=0, color='k')
										ax.set_ylabel(details['long_name']+' - '+details['rel-abs'])
										ax.set_xlabel('wlvl')
										pdf.savefig(bbox_inches='tight')

					with PdfPages(path_out+'_'.join([iso,var])+'.pdf') as pdf:
						for region in region_names:
							for aggr,masks in mask_dict.items():
								for season,months in season_dict.items():
									plt.close('all')
									fig,axes = plt.subplots(nrows=2, sharex=True, gridspec_kw={'height_ratios':[2,4]})
									x = np.array(out_median.wlvl,np.float)

									devs = deviation.loc[region,:,aggr,season,:]
									# filter out gcm/im combinations that only exist in ref
									devs = devs[:,np.isfinite(devs.loc['1.5'].values)]

									axes[0].set_title(' '.join([region,aggr,season]))
									axes[0].plot(x, np.isfinite(devs).sum('ID'))
									axes[0].set_ylabel('ensemble size')

									axes[1].plot(x, out_median.loc[region,:,aggr,season])
									axes[1].fill_between(x, out_median.loc[region,:,aggr,season]+lower_bound.loc[region,:,aggr,season], out_median.loc[region,:,aggr,season]+upper_bound.loc[region,:,aggr,season], alpha=0.5)
									axes[1].set_ylabel(details['long_name']+' - '+details['rel-abs'])
									pdf.savefig(bbox_inches='tight')




#
