import os,sys, importlib, glob,gc, operator

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd

# dis ec2 ec3 flddph fldfrc hursAdjust lec lef lew mindis ped peh prAdjust psAdjust qs rsdsAdjust snd tasAdjust tasminAdjust yield_maize_co2 yield_soy_co2 ec1 flddph_720x360 fldfrc_720x360 hussAdjust led leh maxdis pec pef pew prsnAdjust pslAdjust rldsAdjust sfcWindAdjust soilmoist tasmaxAdjust thawdepth yield_rice_co2 yield_wheat_co2

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['ec1'])
parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

season_dict = {
    'annual' : list(range(1,13)),
    'DJF' : [1,2,12],
    'MAM' : [3,4,5],
    'JJA' : [6,7,8],
    'SON' : [9,10,11],
}


# define warming levels
# wlvls_from_rcp_comparison = np.unique(list(pd.read_csv('meta/scenarios/warming_levels_for_compare_function.csv').iloc[:,2:].values.flatten()))
# wlvls_fixed = np.arange(1.1,3.5,0.1)

# wlvls = np.unique(np.concatenate((wlvls_from_rcp_comparison, wlvls_fixed)))
# wlvls = wlvls[wlvls<3.5]

wlvls = np.arange(1.1,3.5,0.1).round(1)
wlvl_dict = {i+1:str(w) for i,w in enumerate(wlvls)}

for var in args['vars']:
    print(var)
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    if 'excludeImpModels' in list(details.keys()):
        exludeImp = details['excludeImpModels']
    else:
        exludeImp = []

    path_out = '/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/'
    os.system('mkdir -p '+path_out)

    for proj_wlvl_id in sorted(wlvl_dict.keys()):
        proj_wlvl = wlvl_dict[proj_wlvl_id]
        print(proj_wlvl)
        # here I load projection slices
        files_all = glob.glob('/p/tmp/pepflei/impact_data_explorer_data/raw/ec1/*/*/*')
        files = [f for f in files_all if proj_wlvl in f]
        # filter out excluded imp models
        files = [f for f in files if len([i for i in exludeImp if i in f]) == 0]

        out_file = path_out+'_'.join([var,proj_wlvl,'1986-2006','annual'])+'.nc'
        if len(files) > 0:# and (os.path.isfile(out_file)==False or args['overwrite']):
            IDs = [f.split('/')[-1].replace('_'+proj_wlvl+'.nc','').replace('_'+var,'') for f in files]
            proj = xr.open_mfdataset(files, concat_dim='ID')[var]
            proj = proj.assign_coords(ID=IDs)

            diff_all = proj

            # remove infs
            diff_all.values[np.isfinite(diff_all.values) == False] = np.nan

            # diff_all.load()

            # mask irrealistic grid cells.
            # required for crop yields
            if 'irrealistic_values' in details.keys():
                for thresh_dict in details['irrealistic_values']:
                    diff_all.values[thresh_dict['operator'](diff_all.values, thresh_dict['threshold'])] = np.nan

            # prepare to average over rcps
            # IDs_gcm_im -> same length as IDs but with rcp information omitted
            # IDs_gcm_im_unique -> removed duplicates in IDs_gcm_im
            IDs_gcm_im = np.array(['_'.join([i for i in id_.split('_') if 'rcp' not in i]) for id_ in IDs])
            IDs_gcm_im_unique = np.unique(IDs_gcm_im)

            # average over rcps
            gcm_im_combis = []
            for id_ in IDs_gcm_im_unique:
                gcm_im_combis.append(diff_all[IDs_gcm_im == id_].mean('ID'))
            diff = xr.concat(gcm_im_combis, dim='ID')
            diff = diff.assign_coords(ID=IDs_gcm_im_unique)

            # get the ensemble median
            ens_median = diff.median('ID')

            # model agreement
            model_agreement = ens_median.copy() * 0.0
            for ID in diff.ID.values:
                model_agreement += np.sign(diff.loc[ID]) == np.sign(ens_median)
            model_agreement /= float(len(IDs_gcm_im_unique))

            ds = xr.Dataset({'var':ens_median.astype('float32'), 'model_agreement':model_agreement.astype('float32')})
            ds.attrs = {'GCM-IM input':IDs_gcm_im_unique, 'GCM-IM-RCP available':list(IDs)}
            ds.to_netcdf(out_file, format='netCDF4', engine='netcdf4', encoding={'var':{'dtype':'float32',"zlib": True}, 'model_agreement':{'dtype':'float32',"zlib": True}})
            gc.collect()

