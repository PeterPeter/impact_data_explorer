import os,sys, importlib, glob,gc
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import json

import cartopy.io.shapereader as shapereader

os.chdir('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data')

sys.path.append('/Users/peterpfleiderer/Projects/online_tools/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

isos = ['DEU']

for iso in isos:
	print(iso)
	COU = class_on_shape.shaped_object(iso=iso, working_directory='masks/'+iso)

	if len(glob.glob('masks/'+iso+'/masks/*')) == 0:
		print('create mask first')

		os.system('cp -r '+'regioClim_2p0/'+iso+'/'+iso+'_adm_shp '+COU._working_dir+'/'+iso+'_adm_shp')

		shape0 = glob.glob('masks/'+iso+'/*adm*_shp/*0.shp')[0]
		if os.path.isfile(shape0):
			COU.read_shapefile(shape0)
			COU.read_shapefile(shape0, long_name='NAME_0', short_name='GID_0')
			shape1 = glob.glob('masks/'+iso+'/*adm*_shp/*1.shp')[0]
			if os.path.isfile(shape1):
				COU.read_shapefile(shape1)
				COU.read_shapefile(shape1, long_name='NAME_1', short_name='GID_1')

			COU.load_shapefile()

			asdas

			COU.create_masks_overlap(input_file='MIROC5_rcp85_pr_2.7.nc')
			COU.create_masks_latweighted()




#
