
import os,sys,glob,gc,importlib
from os import listdir
from os.path import isfile, join
import numpy as np
import xarray as xr
import pandas as pd
#import xesmf as xe

#for fldfrc
#details = {'overwrite': 'no', 'long_name': 'Flood Fraction', 'orig_name': 'fldfrc', 'short_name': 'fldfrc', 'source': 'ISIMIP - Extreme Events', 'correction_factor': 100.0, 'rel-abs': 'absolute', 'temporal_resolution': 'yearly', 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/fldfrc/*/*/*/*', 'in_files': []}
#for fldph
#details = {'overwrite': 'no', 'long_name': 'Flood Depth', 'orig_name': 'flddph', 'short_name': 'flddph', 'source': 'ISIMIP - Extreme Events', 'rel-abs': 'relative', 'temporal_resolution': 'yearly', 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/flddph/*/*/*/*', 'in_files': []}


#os.chdir('/Users/niklasschwind/Documents/impact_data_explorer')

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v','--var', help='variable', default='fldfrc')
parser.add_argument('-g','--gcm', help='gcm', required=False, default=None)
parser.add_argument('-s','--scenario', help='scenario', required=False, default=None)
parser.add_argument('-m','--mode', help='only test?', required=False, default='run')
parser.add_argument('-w','--wlvls', help='warming levels', nargs='+', default=[])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

# this is the variable id
short_name = args['var']

# if nothing is sspecified -> use all gcms
if args['gcm'] is not None:
    gcms = [args['gcm']]
else:
    gcms = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']

# if nothing is specified -> use all scenarios
if args['scenario'] is not None:
    scenarios = [args['scenario']]
else:
    scenarios = ['rcp26','rcp45','rcp60','rcp85']

overwrite = args['overwrite']
# here you can set 'test' which will break after one file is written
mode = args['mode']
selected_wlvls = args['wlvls']

'''
for testing:
    
short_name = 'qs'
gcms = ['GFDL-ESM2M','HadGEM2-ES','IPSL-CM5A-LR','MIROC5']
scenarios = ['historical','rcp26','rcp45','rcp60','rcp85']
mode = 'run'
overwrite = False
selected_wlvls = []
'''


# get the details about the variable
#this will give a dict called "details" which has information about paths of input files, temporal aggregation method etc.
#sys.path.append('meta/variables/')
#exec("import %s; importlib.reload(%s); from %s import *" % tuple([short_name+'_check']*3))
details = {'overwrite': 'no', 'long_name': 'Flood Fraction', 'orig_name': 'fldfrc', 'short_name': 'fldfrc', 'source': 'ISIMIP - Extreme Events', 'correction_factor': 100.0, 'rel-abs': 'absolute', 'temporal_resolution': 'yearly', 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/fldfrc/*/*/*/*', 'in_files': []}


# get all files that could be relevant in one list
if 'in_files' in details.keys():
    files_all = details['in_files']
else:
    files_all = glob.glob(details['in_files_pattern_historical']) + glob.glob(details['in_files_pattern_future'])

# get a list of impact models
impModels = np.unique([ff.split('_')[-3] for ff in files_all])

# load warming levels (wlvls) and store them in a xarray
wlvlYears_csv = pd.read_csv('/home/niklas/climate-impact-explorer-staging/impact_data_explorer/meta/warming_lvls_cmip5_21_years.csv')
wlvls = np.array(wlvlYears_csv.wlvl.round(1), str)
wlvlYears = xr.DataArray(coords={'gcm':gcms, 'scenario':scenarios, 'wlvl':list(wlvls)}, dims=['gcm','scenario','wlvl'])
for gcm in gcms:
    for scenario in [scen for scen in scenarios if scen!='historical']:
        for wlvl in wlvls:
            wlvlYears.loc[gcm,scenario,wlvls] = wlvlYears_csv[gcm+'_'+scenario]

# do it for all wlvls if no wlvls were specified
if len(selected_wlvls) == 0:
    selected_wlvls = wlvlYears.wlvl.values

print(short_name,gcms,scenarios,overwrite,mode,selected_wlvls)

# go through gcms, rcps, wlvls, and imp models

gcm = ['GFDL-ESM2M','HadGEM2-ES', 'IPSL-CM5A-LR', 'MIROC5']
scenario = ['rcp26', 'rcp60', 'rcp85']
impModels = ['clm45','cwatm','h08','lpjml','matsiro','watergap2']
details =  {'overwrite': 'no', 'long_name': 'Flood Fraction', 'orig_name': 'fldfrc', 'short_name': 'fldfrc', 'source': 'ISIMIP - Extreme Events', 'rel-abs': 'relative', 'temporal_resolution': 'yearly', 'wlvl_files_pattern': '/home/niklas/climate-impact-explorer-staging/rawdata/raw/fldfrc/*/*/*/*', 'in_files': []}
files_all = [str(file) for file in listdir('/home/niklas/climate-impact-explorer-staging/rawdata/fldfrc') if isfile(join('/home/niklas/climate-impact-explorer-staging/rawdata/fldfrc', file))]
print(files_all)
overwrite = False
for gcm in gcms:
    for scenario in scenarios:
        for impModel in impModels:
            # select the relevant files
            files_sel = [ff for ff in files_all if gcm in ff]
            files_sel = [ff for ff in files_sel if impModel in ff]
            files_scen = [ff for ff in files_sel if scenario in ff]

            # if there are some files for this gcm-rcp-im combination
            if len(files_scen) > 0:
                # here there is always one file per scenario
                required_file = files_scen[0]



                data = xr.open_dataset('/home/niklas/climate-impact-explorer-staging/rawdata/fldfrc/'+required_file)[details['orig_name']]
                print('lol')
                for wlvl in selected_wlvls:
                    outPath = '/mnt/PROVIDE/niklas/fldfrc_reproduction/raw/'+'/'.join([short_name,gcm,scenario,impModel])+'/'
                    if os.path.isdir(outPath) == False:
                        os.system('mkdir -p '+outPath)
                    outFile = outPath+'_'.join([gcm,scenario,impModel,short_name,wlvl])+'.nc'
                    if os.path.isfile(outFile) == False or overwrite:
                        if np.isfinite(wlvlYears.loc[gcm,scenario,wlvl]):
                            # get a list of years that are required for the wlvl
                            years = np.arange(wlvlYears.loc[gcm,scenario,wlvl]-10,wlvlYears.loc[gcm,scenario,wlvl]+11,1)

                            # get warming level slice
                            wlvlSlice = data[np.isin(data.time.dt.year.values,years),:,:].mean('time')


                            print('writing: '+outFile)
                            # save the warming level slice in original grid
                            xr.Dataset({details['short_name']:wlvlSlice}).to_netcdf(outFile)

                            if mode == 'test':
                                print('working ' + outFile)
                                asdas

                    else:
                        print('allready done ' + outFile)





#
