import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader
import statsmodels.formula.api as smf


import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-i','--isos', help='isos', nargs='+', default=[])
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['soilmoist'])

parser.add_argument('--subregions', dest='subregions', action='store_true')
parser.add_argument('--no-subregions', dest='subregions', action='store_false')
parser.set_defaults(subregions=True)

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

# in case no iso was specified -> take all isos
if len(args['isos']) == 0:
	cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
	isos = [info.attributes['adm0_a3'] for info in cous]
else:
	isos = args['isos']

# dict { season name : list(months) }
season_dict = {
	'annual' : list(range(1,13)),
	'DJF' : [1,2,12],
	'MAM' : [3,4,5],
	'JJA' : [6,7,8],
	'SON' : [9,10,11],
}

# this is goinf to be initialized with the first wlvl
iso_dict = {}

for var in args['vars']:
    print(var)

    # load details about the variable
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    if 'excludeImpModels' in list(details.keys()):
        exludeImp = details['excludeImpModels']
    else:
        exludeImp = []

    # here I load reference slices
    # I load them several times - once for each RCP
    # I need this to be able to easily calculate changes later on
    files_all = glob.glob(details['wlvl_files_pattern'])
    files = [f for f in files_all if '1986-2006' in f]    
    # filter out excluded imp models
    files = [f for f in files if len([i for i in exludeImp if i in f]) == 0]
    # IDs are all the information about a slice except the warming level (gcm, rcp, impModel)
    IDs = [f.split('/')[-1].replace('_'+'1986-2006'+'.nc','').replace('_'+var,'') for f in files]

    # get files for 1.5°C to check which models are excluded
    files_all = glob.glob(details['wlvl_files_pattern'])
    files_proj = [f for f in files_all if '1.5' in f]
    # filter out excluded imp models
    files_proj = [f for f in files_proj if len([i for i in exludeImp if i in f]) == 0]
    proj_IDs = [f.split('/')[-1].replace('_1.5.nc','').replace('_'+var,'').replace('_rcp26','').replace('_rcp45','').replace('_rcp60','').replace('_rcp85','') for f in files_proj]

    # here I make a selection of IDs that are available in ref and proj
    IDs = [id_ for id_ in IDs if id_.replace('_historical','') in proj_IDs]

    # load only data that is in IDs
    datas = []
    for fl in files:
        if fl.split('/')[-1].replace('_'+'1986-2006'+'.nc','').replace('_'+var,'') in IDs:
            datas.append(xr.load_dataset(fl)[var])
    ref = xr.concat(datas, dim='ID')
    ref = ref.assign_coords(ID=IDs)

    ref = ref.loc[IDs]

    # load the data into the RAM
    ref.load()

    # this is a correction required for some specific data (led, ped, .. etc.)
    if 'correction_factor' in details.keys():
        ref *= float(details['correction_factor'])

    # go through countries - somehow I call them isos because I work with 3 letter country codes
    for iso in isos:
        if len(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*')) > 0:
            print(iso)
            # create output path it does not exist
            path_out = '/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'
            os.system('mkdir -p '+path_out)

            # define output file name and only continue if that file does not exist or the option is --overwrite
            file_out = path_out+'_'.join([iso,var])+'_ref.nc'
            if os.path.isfile(file_out) == False or args['overwrite']:

                # load masks
                COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
                COU.load_mask()

                # put masks in new dict
                mask_dict = {
                    'area' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['latWeight'],
                    'pop' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['pop2005'],
                    'gdp' : COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['gdp2005'],
                }
                # I'll take lons and lats from this mask
                mask_overlap = COU._masks['360x720lat89p75to-89p75lon-179p75to179p75']['overlap']

                # depending on --subregions or --no-subregions the region names are loaded
                if args['subregions']:
                    region_names = mask_overlap.coords['region'].values
                else:
                    region_names = [iso]

                # in this array I will store deviations from the median
                ref_av = xr.DataArray(coords={'region':region_names, 'aggregation':list(mask_dict.keys()), 'season':list(season_dict.keys()),'ID':IDs}, dims=['region','aggregation','season','ID'])

                for aggr,masks in mask_dict.items():
                    for region in region_names:

                        # select mask
                        mask = masks.loc[region]

                        # average over area
                        ref_area = (ref.sel(ID=IDs).sel({'lat':mask_overlap.coords['lat'].values, 'lon':mask_overlap.coords['lon'].values}) * mask).sum('lat').sum('lon')

                        # the next block is a simplification for annual data -> check the "else" block for details
                        if details['temporal_resolution'] == 'yearly':
                            ref_av.loc[region,aggr,'annual',:] = ref_area

                        else:
                            for season,months in season_dict.items():
                                # select months of season and aggregate depending on the "seasonal_aggregation" in details
                                ref_av.loc[region,aggr,season,:] = ref_area.loc[:,months].reduce(details['seasonal_aggreagation'], 'month')

                xr.Dataset({'ref':ref_av}).to_netcdf(file_out)
                print('done',iso,file_out)













