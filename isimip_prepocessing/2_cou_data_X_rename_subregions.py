import os,sys, importlib, glob,gc
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import json

import cartopy.io.shapereader as shapereader


sys.path.append('/home/pepflei/Projects/climate_data_on_shapes/')
import class_on_shape; importlib.reload(class_on_shape)

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()

cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
isos = [info.attributes['adm0_a3'] for info in cous] + ['ESH','SSD']
parser.add_argument('-i','--isos', help='isos', nargs='+', default=isos)
parser.add_argument('-v','--vars', help='variables', nargs='+', default=[])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

# get translation dict
for iso in args['isos']:
    try:
        print(iso)

        COU = class_on_shape.shaped_object(iso=iso, working_directory='/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso)
        COU.load_shapefile()

        cous = shapereader.Reader(COU._working_dir+'/'+iso+'_adm_shp/gadm36_'+iso+'_1.shp').records()
        translation_dict = {iso:iso}
        for item in cous:
            tmp,info=item.geometry,item.attributes
            translation_dict[info['GID_1']] = info['HASC_1']



        if len(args['vars']) > 0:
            varis = args['vars']
        else:
            varis = [fl.split('/')[-1] for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/cou_data/*')]
        print(varis)

        # change names in files
        for var in varis:
            files = glob.glob('/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'+iso+'*.nc')
            for fl in files:
                nc_in = xr.load_dataset(fl)
                if list(translation_dict.keys())[-1] in nc_in.region.values:
                    nc_out = nc_in.assign_coords(region = [translation_dict[reg] for reg in nc_in.region.values])
                    nc_out.to_netcdf(fl)

        # change names in masks
        files = [fl for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/masks/*.nc4') if 'oldNames' not in fl]
        for fl in files:
            os.system('cp '+fl+' '+fl.replace('.nc','_oldNames.nc'))
            nc_in = xr.open_dataset(fl.replace('.nc','_oldNames.nc'))
            nc_o = xr.Dataset({})

            for var in list(nc_in.variables.keys()):
                if '.' in var:
                    nc_o[translation_dict[var]] = nc_in[var]

                else:
                    nc_o[var] = nc_in[var]

            nc_o.to_netcdf(fl)

    except:
        print(iso)
        print('issue')




#
