import os,sys, importlib, glob,gc
import numpy as np
import xarray as xr

# for plotting
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib as mpl
import cartopy.crs as ccrs

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['ec2_720x360'])
parser.add_argument('-w','--wlvls', help='warming levels', nargs='+', default=['1.5','2.0','3.0'])
parser.add_argument('-r','--ref', help='reference warming level', default='1.1', type=str)


parser.add_argument('-i','--isos', help='isos', nargs='+', default=['DEU','CHN','BEN'])

parser.add_argument('-c','--colorScale', help='colorScale', nargs='+', default=None)
parser.add_argument('-ct','--colorType', help='colorType', default='RdBu_r')

parser.add_argument('--rawData', dest='rawData', action='store_true')
parser.add_argument('--no-rawData', dest='rawData', action='store_false')
parser.set_defaults(rawData=False)

parser.add_argument('--globMaps', dest='globMaps', action='store_true')
parser.add_argument('--no-globMaps', dest='globMaps', action='store_false')
parser.set_defaults(globMaps=True)

parser.add_argument('--couData', dest='couData', action='store_true')
parser.add_argument('--no-couData', dest='couData', action='store_false')
parser.set_defaults(couData=True)

parser.add_argument('--subregions', dest='subregions', action='store_true')
parser.add_argument('--no-subregions', dest='subregions', action='store_false')
parser.set_defaults(subregions=False)

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)

######################
# Agreement color map
######################

norm = mpl.colors.Normalize(0,1)
cmap1 = mpl.colors.LinearSegmentedColormap.from_list("", ['yellow','red','m'])
cmap2 = mpl.colors.LinearSegmentedColormap.from_list("", ['white','green','blue'])
colors = np.vstack((cmap2(np.linspace(0, 1, 66)), cmap1(np.linspace(0, 1, 33))))
mymap = mpl.colors.LinearSegmentedColormap.from_list('mixed_cmap', colors)



###################
# raw data
###################

for var in args['vars']:
    print(var)
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    files = glob.glob(details['wlvl_files_pattern'])

    if args['rawData']:
        gcms = np.unique([fl.split('/')[7] for fl in files])
        rcps = np.unique([fl.split('/')[8] for fl in files])
        ims = np.unique([fl.split('/')[9] for fl in files if '.nc' not in fl.split('/')[9]])

        if len(ims) > 0:
            found = xr.DataArray(0, coords={'gcm':gcms, 'rcp':rcps, 'im':ims, 'wlvl':['1986-2006'] + [str(w) for w in np.arange(1.0,5.0,0.1).round(1)]}, dims=['gcm','rcp','wlvl','im'])
        if len(ims)==0:
            found = xr.DataArray(0, coords={'gcm':gcms, 'rcp':rcps, 'wlvl':['1986-2006'] + [str(w) for w in np.arange(1.0,5.0,0.1).round(1)]}, dims=['gcm','rcp','wlvl'])
            
        for gcm in gcms:
            for rcp in rcps:
                for wlvl in found.wlvl.values:
                    if os.path.isfile('/p/tmp/pepflei/impact_data_explorer_data/raw/'+var+'/'+gcm+'/'+rcp+'/'+gcm+'_'+rcp+'_'+var+'_'+wlvl+'.nc'):
                        found.loc[gcm,rcp,wlvl] = 1

                    if len(ims) > 0:
                        for im in ims:
                            if os.path.isfile('/p/tmp/pepflei/impact_data_explorer_data/raw/'+var+'/'+gcm+'/'+rcp+'/'+im+'/'+gcm+'_'+rcp+'_'+im+'_'+var+'_'+wlvl+'.nc'):
                                found.loc[gcm,rcp,wlvl,im] = 1

        os.system('mkdir -p /p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/')

        if len(ims)==0:
            fig,axes = plt.subplots(nrows=4, figsize=(12,8))
            for gcm,ax in zip(gcms,axes):
                ax.set_title(gcm)
                ax.imshow(found.loc[gcm])
                ax.set_yticks(range(len(found.rcp)))
                ax.set_yticklabels(found.rcp.values,fontsize=6)
                ax.set_xticks(range(len(found.wlvl)))
                ax.set_xticklabels(found.wlvl.values,fontsize=6)
            plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/'+var+'_raw.png')

        if len(ims) > 0:
            for im in ims:
                fig,axes = plt.subplots(nrows=4, figsize=(12,8))
                for gcm,ax in zip(gcms,axes):
                    ax.set_title(gcm)
                    ax.imshow(found.loc[gcm,:,:,im])
                    ax.set_yticks(range(len(found.rcp)))
                    ax.set_yticklabels(found.rcp.values,fontsize=6)
                    ax.set_xticks(range(len(found.wlvl)))
                    ax.set_xticklabels(found.wlvl.values,fontsize=6)
                plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/'+var+'_raw_'+im+'.png')

    ###################
    # wlvl diff
    ###################

    if args['globMaps']:
        if 'ref_mask' in details.keys():
            nc_mask_ref = xr.load_dataset('/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/'+'_'.join([var])+'_ref_mask.nc')
            for season in [s for s in nc_mask_ref.variables if s not in ['lat','lon']]:
                mask = nc_mask_ref[season]
                plt.close('all')
                fig,ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5), subplot_kw={'projection': ccrs.PlateCarree()})
                ax.set_title(var+' mask: '+ nc_mask_ref.attrs['mask'])
                ax.annotate('\n'.join(nc_mask_ref.attrs['IDs']), xy=(0,1), xycoords='axes fraction', fontsize=5, ha='right', va='top')
                ax.coastlines()
                im = ax.contourf(mask.lon, mask.lat, mask, cmap='plasma')
                plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/'+var+'_'+season+'_ref_mask.png', transparent=False, dpi=100, bbox_inches='tight'); plt.close()

        for file_ in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/*'+args['ref']+'*'):
            if file_.split('_')[-3] in args['wlvls']:
                out_file = file_.replace('/wlvl_diff/','/checks/').replace('.nc','.png')
                if os.path.isfile(out_file) or args['overwrite']:

                    nc = xr.open_dataset(file_)
                    if 'ref_mask' in details.keys():
                        season = file_.split('_')[-1].split('.')[0]
                        data = nc['var'] * nc_mask_ref[season]
                    else:
                        data = nc['var']

                    model_agreement = nc['model_agreement']

                    plt.close('all')
                    fig,ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5), subplot_kw={'projection': ccrs.PlateCarree()})
                    ax.set_title(var+' runs: %s' %( len('\n'.join(nc.attrs['GCM-IM input']))) )
                    ax.annotate('\n'.join(nc.attrs['GCM-IM input']), xy=(0,1), xycoords='axes fraction', fontsize=5, ha='right', va='top')
                    ax.coastlines()

                    if args['colorScale'] is None:
                        if data.max() > 0 and data.min() < 0:
                            tmp = data.values
                            tmp[np.isfinite(tmp) == False] = np.nan
                            mabs = np.nanmax(np.abs(np.nanpercentile(tmp, [5,95])))
                            if mabs == 0:
                                mabs = 1
                            levels = np.linspace(-mabs,mabs,100)
                        else:
                            tmp = data.values
                            tmp[np.isfinite(tmp) == False] = np.nan
                            levels = np.linspace(np.nanpercentile(tmp,5),np.nanpercentile(tmp,95),100)
                    else:
                        levels = np.linspace(float(args['colorScale'][0]),float(args['colorScale'][1]),100)

                    img = ax.imshow(np.array([levels]), cmap=args['colorType'])
                    im = ax.contourf(data.lon,data.lat,data.values, cmap=args['colorType'], levels=levels, extend='both')
                    img.set_visible(False)
                    fig.colorbar(img, ax=ax, shrink=0.6, label=file_.split('/')[-1])
                    if os.path.isdir('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var) == False:
                        os.system('mkdir /p/tmp/pepflei/impact_data_explorer_data/checks/'+var)
                    plt.savefig(out_file, transparent=False, dpi=100, bbox_inches='tight')

                    ax.set_extent([-180,180,-90,90])
                    agree_mask = model_agreement.copy() * 0 + 1
                    agree_mask.values[model_agreement.values > 0.66] = np.nan
                    if np.any(np.isfinite(agree_mask)):
                        im = ax.contourf(data.lon, data.lat, agree_mask.values, cmap='Greys', vmin=0.5, vmax=1.5, extend='both')
                        plt.savefig(out_file.replace('.png','_mAgree.png'), transparent=False, dpi=100, bbox_inches='tight')

                    plt.close('all')
                    fig,ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5), subplot_kw={'projection': ccrs.PlateCarree()})
                    ax.set_title(var+' runs: %s' %( len('\n'.join(nc.attrs['GCM-IM input']))) )
                    ax.annotate('\n'.join(nc.attrs['GCM-IM input']), xy=(0,1), xycoords='axes fraction', fontsize=5, ha='right', va='top')
                    ax.coastlines()
                    im = ax.contourf(data.lon, data.lat, model_agreement.values, cmap=mymap, levels=np.linspace(0,1,21), extend='both')
                    fig.colorbar(im, ax=ax, shrink=0.6, label=file_.split('/')[-1]+' agreement')
                    plt.savefig(out_file.replace('.png','_agreement.png'), transparent=False, dpi=100, bbox_inches='tight'); plt.close()


    if args['couData']:
        ##############
        # cou data
        ##############

        wlvls = [str(w) for w in list(np.arange(1.0,5.0,0.1).round(1))]

        for iso in args['isos']:
            print(iso)

            nc = xr.load_dataset('/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/' + '_'.join([iso,var])+'.nc')
            out_median = nc['median']
            upper_bound = nc['upper_bound']
            lower_bound = nc['lower_bound']

            if args['subregions']:
                region_names = out_median.region.values
            else:
                region_names = [iso]

            for region in region_names:
                for aggr in out_median.aggregation.values:
                    for season in out_median.season.values:
                        out_file = '/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou/'+'_'.join([var,iso,region,aggr,season])+'.png'
                        if os.path.isfile(out_file) or args['overwrite']:
                            plt.close('all')
                            fig,ax = plt.subplots(nrows=1)
                            x = np.array(out_median.wlvl,np.float)

                            ax.set_title('_'.join([var,iso,region,aggr,season]))

                            ax.plot(x, out_median.loc[:,region,aggr,season])
                            ax.fill_between(x, out_median.loc[:,region,aggr,season]+lower_bound.loc[:,region,aggr,season], out_median.loc[:,region,aggr,season]+upper_bound.loc[:,region,aggr,season], alpha=0.5)
                            ax.set_ylabel(details['long_name']+' - '+details['rel-abs'])

                            if os.path.isdir('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou') == False:
                                os.system('mkdir -p /p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/cou')
                            plt.savefig(out_file, transparent=False,  bbox_inches='tight'); plt.close()
