import os,sys, importlib, glob,gc, argparse, datetime
import numpy as np
import xarray as xr

sys.path.append('meta/variables/')

parser = argparse.ArgumentParser()
parser.add_argument('--v_raw', help='verbocity raw', required=False, type=int, default=0)
parser.add_argument('--v_wlvl', help='verbocity wlvl diff', required=False, type=int, default=0)
parser.add_argument('--v_cou', help='verbocity country data', required=False, type=int, default=0)
parser.add_argument('--var', help='variable', required=False, default=None)
parser.add_argument('--iso', help='variable', required=False, default='DEU')
args = vars(parser.parse_args())

print(args)

'''
args = {'v_raw':0, 'v_wlvl':0, 'v_cou':0, 'var':None}
'''

if args['var'] is None:
    varis = sorted([fl.split('/')[-1] for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/raw/*')])
    done_varis = open('progress_done.txt', 'r').read().split('\n')
    varis = [v for v in varis if v not in done_varis]
else:
    varis = [args['var']]

print(args)

for var in varis:
    print('\n '+var)

    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))
    meta_time = os.path.getmtime('meta/variables/'+var+'_check.py')
    print('meta script: ',datetime.datetime.utcfromtimestamp(meta_time).strftime('%Y-%m-%d %H:%M:%S'))

    files = glob.glob(details['wlvl_files_pattern'])
    gcms = np.unique([fl.split('/')[7] for fl in files])
    rcps = np.unique([fl.split('/')[8] for fl in files])
    ims = np.unique([fl.split('/')[9] for fl in files if '.nc' not in fl.split('/')[9]])
    wlvls = ['1986-2006'] + [str(w) for w in np.arange(1.0,5.6,0.1).round(1)]
    
    print(ims)

    count = 0
    detail_count = {}
    gcm_count = {}
    for gcm in gcms:
        detail_count[gcm] = {}
        gcm_count[gcm] = 0
        for rcp in rcps:
            
            if len(ims) == 0:
                founds = ['NOTHING']
                for wlvl in wlvls:
                    if os.path.isfile('/p/tmp/pepflei/impact_data_explorer_data/raw/'+var+'/'+gcm+'/'+rcp+'/'+gcm+'_'+rcp+'_'+var+'_'+wlvl+'.nc'):
                        founds.append(wlvl)
                        gcm_count[gcm] += 1
                detail_count[gcm][rcp] = len(founds) - 1
                if args['v_raw'] == 3:
                    print(gcm,rcp,founds[-1])
                if args['v_raw'] == 4:
                    print(gcm,rcp,founds)
                if len(founds) > 1:
                    count+=1
            
            if len(ims) > 0:
                detail_count[gcm][rcp] = {}
                for im in ims:
                    detail_count[gcm][rcp][im] = []
                    founds = ['NOTHING']
                    for wlvl in wlvls:
                        if os.path.isfile('/p/tmp/pepflei/impact_data_explorer_data/raw/'+var+'/'+gcm+'/'+rcp+'/'+im+'/'+gcm+'_'+rcp+'_'+im+'_'+var+'_'+wlvl+'.nc'):
                            founds.append(wlvl)
                            gcm_count[gcm] += 1
                    detail_count[gcm][rcp][im] = len(founds) - 1
                    if args['v_raw'] == 3:
                        print(gcm,rcp,im,founds[-1])
                    if args['v_raw'] == 4:
                        print(gcm,rcp,im,founds)
                    if len(founds) > 1:
                        count+=1

    print('number of models %s' %(count))  
    if args['v_raw'] == 1:
        print(gcm_count)
    if args['v_raw'] == 2:
        print(detail_count)

    if count > 0:
        done_with_raw = np.max([os.path.getmtime(file_) for file_ in files])

        if len(ims) > 0 and args['v_raw']==5:
            im_dict = {}
            for wlvl in wlvls:
                IDs = [f.split('/')[-1].replace('_'+wlvl+'.nc','').replace('_'+var,'') for f in [f for f in files if wlvl in f]]
                IDs_gcm_im = np.array(['_'.join([i for i in id_.split('_') if 'rcp' not in i]) for id_ in IDs])
                IDs_gcm_im_unique = np.unique(IDs_gcm_im)
                complete_ims = {}
                for im in ims:
                    # IDs_tmp = [id_ for id_ in IDs if im in id_]
                    # if len(IDs_tmp) > 0:
                    #     print(wlvl, im, {gcm:len([id_ for id_ in IDs_tmp if gcm in id_]) for gcm in gcms})
                    found_gcms = np.unique([id_.split('_')[0] for id_ in IDs if im in id_])
                    complete_ims[im] = len(found_gcms)
                print(wlvl,complete_ims)

        print("last file ", datetime.datetime.utcfromtimestamp(done_with_raw).strftime('%Y-%m-%d %H:%M:%S'))

    # wlvl diff
    if count > 0:
        all_files = sorted(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/*'))
        if len(all_files) > 0:
            started_with_wlvl = np.min([os.path.getmtime(file_) for file_ in all_files])

            wlvl_diffs = ['_'.join(fl.split('_')[-3:-1]) for fl in all_files]

            print('wlvl diff:\t\t%s %s' %(len(wlvl_diffs), {True:'', False:'\t\t!update???'}[done_with_raw<started_with_wlvl and meta_time<started_with_wlvl]))
            if args['v_wlvl'] == 1:
                print(wlvl_diffs)
            
            print("last file ", datetime.datetime.utcfromtimestamp(np.max([os.path.getmtime(file_) for file_ in all_files])).strftime('%Y-%m-%d %H:%M:%S'))
        else:
            print('wlvl diff:\t\t TODO')
            
    # cou data
    if count > 0:
        all_files = sorted(glob.glob('/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/'+args['iso']+'/*.nc'))
        if len(all_files) > 0:
            started_with_regAv = np.min([os.path.getmtime(file_) for file_ in all_files])
            done_with_regAv = np.max([os.path.getmtime(file_) for file_ in all_files])

            isos = [fl.split('/')[-1].split('_')[0] for fl in all_files]
            print('regional averages:\t\t%s\t%s %s' %(args['iso'], len(isos), {True:'', False:'\t\t!update???'}[done_with_raw<started_with_regAv and meta_time<started_with_regAv]))
            if args['v_cou'] == 1:
                print(isos)
            print("last file ", datetime.datetime.utcfromtimestamp(np.max([os.path.getmtime(file_) for file_ in all_files])).strftime('%Y-%m-%d %H:%M:%S'))
        else:
            print('regional averages:\t\t TODO')

        all_files = sorted([fl for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/*.nc') if 'ref' not in fl])
        if len(all_files) > 0:
            started_with_isos = np.min([os.path.getmtime(file_) for file_ in all_files])

            isos = [fl.split('/')[-1].split('_')[0] for fl in all_files]
            print('uncertaintiy bounds:\t\t%s %s' %(len(isos), {True:'', False:'\t\t!update???'}[done_with_regAv<started_with_isos and meta_time<started_with_isos]))
            if args['v_cou'] == 1:
                print(isos)
            print("last file ", datetime.datetime.utcfromtimestamp(np.max([os.path.getmtime(file_) for file_ in all_files])).strftime('%Y-%m-%d %H:%M:%S'))
        else:
            print('uncertaintiy bounds:\t\t TODO')

        all_files = sorted([fl for fl in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/cou_data/'+var+'/*.nc') if 'ref' in fl])
        if len(all_files) > 0:
            started_with_isos = np.min([os.path.getmtime(file_) for file_ in all_files])

            isos = [fl.split('/')[-1].split('_')[0] for fl in all_files]
            print('reference average:\t\t%s %s' %(len(isos), {True:'', False:'\t\t!update???'}[done_with_raw<started_with_isos and meta_time<started_with_isos]))
            if args['v_cou'] == 1:
                print(isos)
            print("last file ", datetime.datetime.utcfromtimestamp(np.max([os.path.getmtime(file_) for file_ in all_files])).strftime('%Y-%m-%d %H:%M:%S'))
        else:
            print('reference average:\t\t TODO')






#
