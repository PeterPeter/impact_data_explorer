import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json

# for plotting
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import cartopy.crs as ccrs

nc = xr.open_dataset('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/agriculture_masks/surfdata_360x720cru_simyr2000_c170614_WT_modified_4_ISIMIP_2005soc.nc')

data = nc['PCT_PFT']

for i in range(25):

    plt.close('all')
    fig,ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5), subplot_kw={'projection': ccrs.PlateCarree()})
    ax.set_title(i)
    ax.coastlines()

    im = ax.contourf(nc['LONGXY'],nc['LATIXY'],data.values[i], cmap='jet', levels=np.linspace(0,100,100))

    fig.colorbar(im, ax=ax, shrink=0.6)
    plt.savefig('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/agriculture_masks/plots/PCT_PFT_'+str(i)+'.png', transparent=False, dpi=100, bbox_inches='tight'); plt.close()
    gc.collect()
