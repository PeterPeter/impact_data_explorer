import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json

# for plotting
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import cartopy.crs as ccrs


import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-v','--vars', help='variables', nargs='+', default=[])
parser.add_argument('-w','--wlvls', help='warming levels', nargs='+', default=['1.5','2.0','2.5','3.0'])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())

print(args)

for var in args['vars']:
	print(var)
	sys.path.append('meta/variables/')
	exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

	for file_ in glob.glob('/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/'+var+'/*1986-2006*'):
		if file_.split('_')[-3] in args['wlvls']:
			nc = xr.open_dataset(file_)
			data = nc['var']

			plt.close('all')
			fig,ax = plt.subplots(nrows=1, ncols=1, figsize=(10,5), subplot_kw={'projection': ccrs.PlateCarree()})
			ax.set_title(var+' runs: %s' %( len('\n'.join(nc.attrs['GCM-IM input']))) )
			ax.annotate('\n'.join(nc.attrs['GCM-IM input']), xy=(0,1), xycoords='axes fraction', fontsize=5, ha='right', va='top')
			ax.coastlines()

			if details['rel-abs'] == 'relative':
				im = ax.contourf(data.lon, data.lat, data.values, cmap='RdBu_r', levels=np.arange(-100,110,10), extend='both')
			else:
				im = ax.contourf(data.lon,data.lat,data.values, cmap='plasma')

			fig.colorbar(im, ax=ax, shrink=0.6, label=file_.split('/')[-1])
			if os.path.isdir('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var) == False:
				os.system('mkdir /p/tmp/pepflei/impact_data_explorer_data/checks/'+var)
			plt.savefig(file_.replace('/wlvl_diff/','/checks/').replace('.nc','.png'), transparent=False, dpi=100, bbox_inches='tight'); plt.close()
			gc.collect()

