import os,sys, importlib, glob,gc
import numpy as np
import xarray as xr

# for plotting
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-v','--vars', help='variables', nargs='+', default=[])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())

for var in args['vars']:
	sys.path.append('meta/variables/')
	exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

	files = glob.glob(details['wlvl_files_pattern'])

	gcms = np.unique([fl.split('/')[7] for fl in files])
	rcps = np.unique([fl.split('/')[8] for fl in files])
	ims = np.unique([fl.split('/')[9] for fl in files if '.nc' not in fl.split('/')[9]])

	if len(ims) > 0:
		found = xr.DataArray(0, coords={'gcm':gcms, 'rcp':rcps, 'im':ims, 'wlvl':['1986-2006'] + [str(w) for w in np.arange(1.0,5.0,0.1).round(1)]}, dims=['gcm','rcp','wlvl','im'])
	if len(ims)==0:
		found = xr.DataArray(0, coords={'gcm':gcms, 'rcp':rcps, 'wlvl':['1986-2006'] + [str(w) for w in np.arange(1.0,5.0,0.1).round(1)]}, dims=['gcm','rcp','wlvl'])
		
	for gcm in gcms:
		for rcp in rcps:
			for wlvl in found.wlvl.values:
				if os.path.isfile('/p/tmp/pepflei/impact_data_explorer_data/raw/'+var+'/'+gcm+'/'+rcp+'/'+gcm+'_'+rcp+'_'+var+'_'+wlvl+'.nc'):
					found.loc[gcm,rcp,wlvl] = 1

				if len(ims) > 0:
					for im in ims:
						if os.path.isfile('/p/tmp/pepflei/impact_data_explorer_data/raw/'+var+'/'+gcm+'/'+rcp+'/'+im+'/'+gcm+'_'+rcp+'_'+im+'_'+var+'_'+wlvl+'.nc'):
							found.loc[gcm,rcp,wlvl,im] = 1

	os.system('mkdir -p /p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/')

	if len(ims)==0:
		fig,axes = plt.subplots(nrows=4, figsize=(12,8))
		for gcm,ax in zip(gcms,axes):
			ax.set_title(gcm)
			ax.imshow(found.loc[gcm])
			ax.set_yticks(range(len(found.rcp)))
			ax.set_yticklabels(found.rcp.values,fontsize=6)
			ax.set_xticks(range(len(found.wlvl)))
			ax.set_xticklabels(found.wlvl.values,fontsize=6)
		plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/'+var+'_raw.png')

	if len(ims) > 0:
		for im in ims:
			fig,axes = plt.subplots(nrows=4, figsize=(12,8))
			for gcm,ax in zip(gcms,axes):
				ax.set_title(gcm)
				ax.imshow(found.loc[gcm,:,:,im])
				ax.set_yticks(range(len(found.rcp)))
				ax.set_yticklabels(found.rcp.values,fontsize=6)
				ax.set_xticks(range(len(found.wlvl)))
				ax.set_xticklabels(found.wlvl.values,fontsize=6)
			plt.savefig('/p/tmp/pepflei/impact_data_explorer_data/checks/'+var+'/'+var+'_raw_'+im+'.png')

	#
