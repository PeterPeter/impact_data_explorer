#!/bin/bash

#SBATCH --qos=short
#SBATCH --partition=standard
#SBATCH --job-name=seasons
##SBATCH --account=climber3
##SBATCH --output=logs/%A

##SBATCH --ntasks=4
#SBATCH --cpus-per-task=8
##SBATCH --nodes=4-4
##SBATCH --ntasks-per-node=4

##SBATCH --array=0-86%20

#####################################################################

python "$@"
