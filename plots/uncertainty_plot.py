import os,sys, importlib, glob,gc,time
import numpy as np
import xarray as xr
import pandas as pd

special_wlvls = [str(w) for w in [1.5,2.0,2.5,3.0,3.5]]

os.chdir('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/api')
data_path = '/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data'

from scenarios import *
from prepare_all_meta import *
from dummies import *

meta = json.load(open('../meta/meta_info.json'))

iso = 'DEU'
region = 'DEU'
var = 'prAdjust'
scenario = 'ngfs_ndc'
season = 'annual'
aggregation_spatial = 'area'

print(iso,region,var,scenario,season)

# load impact for warming levels
nc = xr.load_dataset(data_path+'/cou_data/'+var+'/'+iso+'_'+var+'.nc')
data = nc['median']
upper_bound = nc['upper_bound'] + nc['median']
lower_bound = nc['lower_bound'] + nc['median']

# get projection
yrWlvl = scenarios[scenario]

out = pd.DataFrame(columns = ['year','wlvl','median','upper','lower'])
out['year'] = np.arange(2015,2100,5)

# needs fixes :( not elegant
for wlvl in [str(w) for w in np.arange(1,3.5,0.1).round(1)] + ['1.5','2.0','2.5','3.0']:
	# CHECK AGAIN
	actual_year = yrWlvl.loc[yrWlvl['wlvl median'] == wlvl,'year'].values
	if len(actual_year) > 0:
		actual_year = actual_year[0]
		if actual_year <= 2095:
			yr = out.year[np.argsort(np.abs(np.array(out.year, np.float)-actual_year))[0]]
			out.loc[out.year == yr, 'wlvl'] = wlvl

# NO INTERPOLATION
for yr in out.year:
	wlvl = np.float(yrWlvl.loc[yrWlvl.year==yr, 'wlvl median'].values[0])
	wlvls = [str(round(w,1)) for w in [wlvl-0.1,wlvl,wlvl+0.1] if w >=1.0 and w <= 3.4]
	out.loc[out.year == yr,'median'] = np.float(data.loc[region, wlvls, aggregation_spatial, season].mean('wlvl'))

	upper_wlvl = np.float(yrWlvl.loc[yrWlvl.year==yr, 'wlvl p95'].values[0])
	upper_wlvls = [str(round(w,1)) for w in [upper_wlvl-0.1,upper_wlvl,upper_wlvl+0.1] if w >=1.0 and w <= 3.4]
	if len(upper_wlvls) == 0:
		upper_wlvls = ['3.4']
	lower_wlvl = np.float(yrWlvl.loc[yrWlvl.year==yr, 'wlvl p5'].values[0])
	lower_wlvls = [str(round(w,1)) for w in [lower_wlvl-0.1,lower_wlvl,lower_wlvl+0.1] if w >=1.0 and w <= 3.4]
	if len(lower_wlvls) == 0:
		lower_wlvls = ['3.4']

	print(upper_wlvl,upper_wlvls)

	possible_combis = []
	for wlvls_ in [upper_wlvls,lower_wlvls,wlvls]:
		for bound_ in [upper_bound,lower_bound,data]:
			possible_combis.append(np.float(bound_.loc[region, wlvls_, aggregation_spatial, season].mean('wlvl')))
	out.loc[out.year == yr,'upper'] = np.nanmax(possible_combis)
	out.loc[out.year == yr,'lower'] = np.nanmin(possible_combis)

	possible_combis = []
	for bound_ in [upper_bound,lower_bound,data]:
		possible_combis.append(np.float(bound_.loc[region, wlvls, aggregation_spatial, season].mean('wlvl')))
	out.loc[out.year == yr,'upper impact'] = np.nanmax(possible_combis)
	out.loc[out.year == yr,'lower impact'] = np.nanmin(possible_combis)

	possible_combis = []
	for wlvls_ in [upper_wlvls,lower_wlvls,wlvls]:
		possible_combis.append(np.float(data.loc[region, wlvls_, aggregation_spatial, season].mean('wlvl')))
	out.loc[out.year == yr,'upper wlvl'] = np.nanmax(possible_combis)
	out.loc[out.year == yr,'lower wlvl'] = np.nanmin(possible_combis)



import matplotlib.pyplot as plt
import seaborn as sns

plt.close('all')
plt.plot(out.year, out['median'])
plt.fill_between(out.year, np.array(out['upper'],np.float), np.array(out['lower'],np.float), alpha=0.3, label='full')
plt.fill_between(out.year, out['upper impact'], out['lower impact'], alpha=0.3, label='from impact model projections')
plt.fill_between(out.year, out['upper wlvl'], out['lower wlvl'], alpha=0.3, label='from GMT projections')
plt.legend(title='uncertainty', loc='upper left')
plt.ylabel(var)
plt.savefig(data_path+'uncertainties_'+var+'.png')



'''
1.5.3 Data replacement for high warming - only in beta version
At the moment we have computed impacts up to 3.0°C GMT. The upper bound of uncertainty however includes warming levels above 3.0°C. In the beta version impacts for GMT > 3.0°C are replaced by the impact at 3.0°C. This feature only affects the upper bound of projections in the years 2080-2100.


'''


#