import os,sys, importlib, glob,gc,time
import flask
from flask import request, jsonify, make_response
import numpy as np
import xarray as xr
import pandas as pd
import json,io

# this is a nice way of getting the error message when something goes wrong in a "try: " block
import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# here I test whether the api is run on the server or on my computer and adjust the paths accordingly
possible_paths = [
    '/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/',
    '/home/tooli/impact_data_explorer/data/'
]
for data_path in possible_paths:
    if os.path.isdir(data_path):
        break

# should be changed to something else than "DEBUG" at some point?
app = flask.Flask(__name__)
app.config["DEBUG"] = True

# import scenario information, the meta information, and dummy dicts that are given out if something went wrong
from scenarios import *
from prepare_all_meta import *
from dummies import *

# add the information about scenarios into the meta dict
meta['scenarios'] = scenarios_meta



iso = 'CAN'
region = 'CAN'
var = 'flddph'
wlvls = ['3.0']
scenario = 'RCP45'
season = 'annual'
aggregation_spatial = 'other'
mode = 'json'









