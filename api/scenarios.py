import flask, math
from flask import request, jsonify

import os,sys, importlib, glob,gc,time
import numpy as np
import xarray as xr
import pandas as pd
from pathlib import Path
import json

possible_paths = [
    '/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/',
    '/home/tooli/impact_data_explorer/data/',
    '/Users/niklasschwind/Documents/impact_data_explorer/data/'
]

for data_path in possible_paths:
    if os.path.isdir(data_path):
        break


ngfs_data = pd.read_table(Path(data_path+'../meta/scenarios/climate_data_p4_all_models.csv'), sep=';')
#if data_path == '/home/tooli/impact_data_explorer/data/':
#    ngfs_data = pd.read_table('/home/tooli/impact_data_explorer/meta/scenarios/NGFS_scenario_data_June2022.csv', sep=';')
#else:
#    ngfs_data = pd.read_table('/Users/niklasschwind/Documents/impact_data_explorer/meta/scenarios/NGFS_scenario_data_June2022.csv', sep=';')

scenarios = {}
scenarios_meta = [
    {'id' : 'rcp26', 'name' : 'RCP2.6', 'basescenario' : False, 'primary':True, 'description' : 'The Representative Concentration Pathway 2.6 is one of the four core greenhouse gas concentration pathways assessed by the IPCC in its fifth Assessment Report (AR5). It assumes that CO2 emissions start declining by 2020 and reach zero by 2100, leading to a radiative forcing (change in energy flux to the atmosphere due to human activities) of 2.6 W/m2 by 2100. This scenario is likely (66-90% chance) to limit global warming below 2°C by 2100.'},
    {'id' : 'rcp45', 'name' : 'RCP4.5', 'basescenario' : False, 'primary':True, 'description' : 'The Representative Concentration Pathway 4.5 is one of the four core greenhouse gas concentration pathways assessed by the IPCC in its fifth Assessment Report (AR5). It assumes that CO2 emissions start declining by approximately 2045 and reach roughly half of their 2050 levels by 2100, leading to a radiative forcing (change in energy flux to the atmosphere due to human activities) of 4.5 W/m2 by 2100. RCP 4.5 is more likely than not to result in global temperature rise between 2 °C and 3 °C.'},
    {'id' : 'rcp60', 'name' : 'RCP6.0', 'basescenario' : False, 'primary':True, 'description' : 'The Representative Concentration Pathway 6.0 is one of the four core greenhouse gas concentration pathways assessed by the IPCC in its fifth assessment report (AR5). Global emissions are assumed to peak around 2080 then decline, leading to a radiative forcing (change in energy flux to the atmosphere due to human activities) of 6.0 W/m2 and to a global temperature rise by about 3-4°C by 2100.'},
    {'id' : 'rcp85', 'name' : 'RCP8.5', 'basescenario' : False, 'primary':True, 'description' : 'The Representative Concentration Pathway 8.5 is one of the four core greenhouse gas concentration pathways assessed by the IPCC in its fifth assessment report (AR5). It assumes that global emissions continue to rise throughout the 21st century, leading to a global mean temperature rise of close to 4°C in 2100.'},
    {'id' : 'h_cpol', 'name' : 'NGFS current policies', 'basescenario' : False, 'primary':True, 'description' : 'This scenario assumes that only currently implemented climate policies are maintained, with no further strengthening. Global greenhouse gas emissions grow until 2080, leading to about 3 °C of warming and irreversible changes like higher sea level rise. It was developed for the Network for Greening the Financial System (NGFS). In the NGFS terminology, this is considered a “hot house” scenario, characterised by high physical risks, but low transition risks. Find out more in the Methodology or on the NGFS Scenarios Portal: "https://www.ngfs.net/ngfs-scenarios-portal/explore/":https://www.ngfs.net/ngfs-scenarios-portal/explore/'},
    {'id' : 'o_1p5c', 'name' : 'NGFS net-zero 2050', 'basescenario' : False, 'primary':True, 'description' : 'This ambitious scenario, that was developed for the Network for Greening the Financial System (NGFS), limits global warming to 1.5 °C through immediate introduction of stringent climate policies and innovation, reaching net zero CO₂ emissions globally around 2050. Some jurisdictions such as the US, EU and Japan reach net zero for all greenhouse gases by this point. Carbon Dioxide Removal (CDR) is used to accelerate the decarbonisation but kept to the minimum possible and broadly in line with sustainable levels of bioenergy production. Physical risks are relatively low but transition risks are high. Find out more in the Methodology or on the NGFS Scenarios Portal: "https://www.ngfs.net/ngfs-scenarios-portal/explore/":https://www.ngfs.net/ngfs-scenarios-portal/explore/'},
    {'id': 'd_strain', 'name': 'NGFS fragmented world', 'basescenario': False, 'primary': True,
     'description': 'This scenario assumes a delayed and divergent climate policy response among countries globally, leading to high physical and transition risks. Countries without zero targets follow current policies, while other countries achieve them only partially (80% of the target). Find out more in the Methodology or on the NGFS Scenarios Portal: "https://www.ngfs.net/ngfs-scenarios-portal/explore/":https://www.ngfs.net/ngfs-scenarios-portal/explore/'},
    {'id': 'h_ndc', 'name': 'NGFS nationally determined contributions', 'basescenario': False, 'primary': True,
     'description': 'This scenario includes all pledged targets even if not yet backed up by implemented effective policies. Find out more in the Methodology or on the NGFS Scenarios Portal: "https://www.ngfs.net/ngfs-scenarios-portal/explore/":https://www.ngfs.net/ngfs-scenarios-portal/explore/'},
    {'id': 'o_2c', 'name': 'NGFS below 2 degree', 'basescenario': False, 'primary': True,
     'description': 'This scenario gradually increases the stringency of climate policies, giving a 67% chance of limiting global warming to below 2°C. Find out more in the Methodology or on the NGFS Scenarios Portal: "https://www.ngfs.net/ngfs-scenarios-portal/explore/":https://www.ngfs.net/ngfs-scenarios-portal/explore/'},
    {'id': 'o_lowdem', 'name': 'NGFS low demand', 'basescenario': False, 'primary': True,
     'description': 'This scenario assumes that significant behavioural changes, reducing energy demand, mitigate the pressure on the economic system to reach global net zero CO2 emissions round 2050. Find out more in the Methodology or on the NGFS Scenarios Portal: "https://www.ngfs.net/ngfs-scenarios-portal/explore/":https://www.ngfs.net/ngfs-scenarios-portal/explore/'},
    {'id' : 'd_delfrag', 'name' : 'NGFS delayed transition', 'basescenario' : False, 'primary':True, 'description' : 'This scenario was developed for the Network for Greening the Financial System (NGFS), assuming new climate policies are not introduced until 2030 and the level of action differs across countries and regions based on currently implemented policies, leading to a “fossil recovery” out of the economic crisis brought about by COVID-19. The availability of Carbon Dioxide Removal (CDR) technologies is assumed to be low, therefore global emissions decline very rapidly after 2030 to ensure a 67 % chance of limiting global warming to below 2 °C in 2100. This leads to both higher transition and physical risks than the Net Zero 2050 scenario, but lower physical risk than the Current Policies scenario. Find out more in the Methodology or on the NGFS Scenarios Portal: "https://www.ngfs.net/ngfs-scenarios-portal/explore/":https://www.ngfs.net/ngfs-scenarios-portal/explore/'},
    {'id' : 'cat_current', 'name' : 'CAT current policies', 'basescenario' : True, 'primary':True, 'description' : 'This scenario explores the consequences of continuing along the path of implemented climate policies in 2020, with no further government action. Global emissions remain high until mid-century and slowly decline after that, leading to about 2.7°C of global warming by 2100 (best estimate). It was developed by an independent scientific analysis called the Climate Action Tracker (CAT), that monitors government climate action of 39 countries representing 85% of global emissions. Find out more in the Methodology or on the Climate Action Tracker website: "https://climateactiontracker.org/about/":https://climateactiontracker.org/about/.'},
]

#Scenarios get adjusted to display on average 1.07 degree of warming in 2010-2020 range
adjustment = {}
###########
# rcp ....
###########
# data = pd.read_table('../meta/scenarios/warming_levels_for_compare_function.csv', sep=',', header=0)
# for rcp in data.scenario:
#     scenarios[rcp] = pd.DataFrame()
#     scenarios[rcp]['year'] = [2030,2050,2070]
#     scenarios[rcp]['wlvl median'] = [str(w) for w in data.loc[data.scenario == rcp].iloc[0,2:].values]

for scenario in ['rcp26','rcp45','rcp60','rcp85']:
    tmp = pd.read_table(Path(data_path+'../meta/scenarios/nauels17gmd_MAGICC_GMT_'+scenario.upper()+'.csv'), sep=';', header=0)
    scenarios[scenario] = pd.DataFrame()
    adjustment[scenario] = str(sum(tmp.loc[tmp['Percentile / Year'] == '50th', [str(int(y)) for y in range(2011,2021)]].values.flatten()) / 10 - 1.09)
    scenarios[scenario]['year'] = list(range(2000,2101))
    scenarios[scenario]['median'] = tmp.loc[tmp['Percentile / Year'] == '50th',[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p5'] = tmp.loc[tmp['Percentile / Year'] == '5th',[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p95'] = tmp.loc[tmp['Percentile / Year'] == '95th',[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['median'] = [str(float(w) - float(adjustment[scenario])) for w in scenarios[scenario]['median']]
    scenarios[scenario]['p5'] = [str(float(w) - float(adjustment[scenario])) for w in scenarios[scenario]['p5']]
    scenarios[scenario]['p95'] = [str(float(w) - float(adjustment[scenario])) for w in scenarios[scenario]['p95']]
    scenarios[scenario]['wlvl median'] = [str(np.round(float(w),1)) for w in scenarios[scenario]['median']]
    scenarios[scenario]['wlvl p5'] = [str(np.round(float(w),1)) for w in scenarios[scenario]['p5']]
    scenarios[scenario]['wlvl p95'] = [str(np.round(float(w),1)) for w in scenarios[scenario]['p95']]


###########
# NGFS ....
###########
'''
IAM = 'REMIND-MAgPIE 2.1-4.2'
IAM = 'MESSAGEix-GLOBIOM 1.0'
# IAM = 'GCAM5.3_NGFS'

for scenario in ['h_cpol','o_1p5c','d_delfrag']:
    med = ngfs_data.loc[(ngfs_data.Model == IAM) & (ngfs_data.Scenario == scenario) & (ngfs_data.Variable == 'Diagnostics|Temperature|Global Mean|MAGICC6|MED')]
    p5 = ngfs_data.loc[(ngfs_data.Model == IAM) & (ngfs_data.Scenario == scenario) & (ngfs_data.Variable == 'Diagnostics|Temperature|Global Mean|MAGICC6|P5')]
    p95 = ngfs_data.loc[(ngfs_data.Model == IAM) & (ngfs_data.Scenario == scenario) & (ngfs_data.Variable == 'Diagnostics|Temperature|Global Mean|MAGICC6|P95')]

    scenarios[scenario] = pd.DataFrame()
    scenarios[scenario]['year'] = list(range(2000,2101))
    scenarios[scenario]['median'] = med.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p5'] = p5.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p95'] = p95.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['wlvl median'] = [str(np.round(w,1)) for w in scenarios[scenario]['median']]
    scenarios[scenario]['wlvl p5'] = [str(np.round(w,1)) for w in scenarios[scenario]['p5']]
    scenarios[scenario]['wlvl p95'] = [str(np.round(w,1)) for w in scenarios[scenario]['p95']]

# scenarios['h_cpol'].loc[np.isin(scenarios['h_cpol'].year,range(2020,2050))]
'''

#TODO: Change the model name to the correct model
#IAM = 'REMIND-MAgPIE 3.0-4.4'
IAM = 'REMIND-MAgPIE 3.2-4.6'
# IAM = 'GCAM5.3_NGFS'

for scenario in ['h_cpol','d_delfrag','o_1p5c', 'd_strain', 'h_ndc', 'o_2c', 'o_lowdem']:
    #TODO: Probably change the variable name to the correct one (you can find it out if you look in the csv file)

    med = ngfs_data.loc[(ngfs_data.model == IAM) & (ngfs_data.scenario == scenario) & (ngfs_data.variable == 'AR6 climate diagnostics|Surface Temperature (GSAT)|MAGICCv7.5.3|50.0th Percentile')]
    p5 = ngfs_data.loc[(ngfs_data.model == IAM) & (ngfs_data.scenario == scenario) & (ngfs_data.variable == 'AR6 climate diagnostics|Surface Temperature (GSAT)|MAGICCv7.5.3|5.0th Percentile')]
    p95 = ngfs_data.loc[(ngfs_data.model == IAM) & (ngfs_data.scenario == scenario) & (ngfs_data.variable == 'AR6 climate diagnostics|Surface Temperature (GSAT)|MAGICCv7.5.3|95.0th Percentile')]
    adjustment[scenario] = sum(med.loc[:,[str(int(y)) for y in range(2011,2021)]].values.flatten()) / 10 - 1.09
    scenarios[scenario] = pd.DataFrame()
    scenarios[scenario]['year'] = list(range(2000,2101))
    scenarios[scenario]['median'] = med.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p5'] = p5.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['p95'] = p95.loc[:,[str(int(y)) for y in  range(2000,2101)]].values.flatten()
    scenarios[scenario]['median'] = [str(float(w) - float(adjustment[scenario])) for w in scenarios[scenario]['median']]
    scenarios[scenario]['p5'] = [str(float(w) - float(adjustment[scenario])) for w in scenarios[scenario]['p5']]
    scenarios[scenario]['p95'] = [str(float(w) - float(adjustment[scenario])) for w in scenarios[scenario]['p95']]
    scenarios[scenario]['wlvl median'] = [str(np.round(float(w),1)) for w in scenarios[scenario]['median']]
    scenarios[scenario]['wlvl p5'] = [str(np.round(float(w),1)) for w in scenarios[scenario]['p5']]
    scenarios[scenario]['wlvl p95'] = [str(np.round(float(w),1)) for w in scenarios[scenario]['p95']]



# scenarios['h_cpol'].loc[np.isin(scenarios['h_cpol'].year,range(2020,2050))]
########
# CAT
########


#runs_high = pd.read_table('../meta/scenarios/MAGICCresults_TRENDHI_CATFINAL_200831/CSSENS_DAT_SURFACE_TEMP_BOXGLOBAL_MAGICC_INDIVIDUAL.OUT', header=17, delim_whitespace=True)
#runs_low = pd.read_table('../meta/scenarios/MAGICCresults_TRENDLO_CATFINAL_200831/CSSENS_DAT_SURFACE_TEMP_BOXGLOBAL_MAGICC_INDIVIDUAL.OUT', header=17, delim_whitespace=True)

runs_high = pd.read_table(Path(data_path+'../meta/scenarios/MAGICCresults_TRENDHI_CATFINAL_200831/CPP_HIGH_CSSENS_DAT_SURFACE_TEMP_BOXGLOBAL_MAGICC_INDIVIDUAL_UPDATED.OUT'), header=17, delim_whitespace=True)
runs_low = pd.read_table(Path(data_path+'../meta/scenarios/MAGICCresults_TRENDLO_CATFINAL_200831/CPP_LOW_CSSENS_DAT_SURFACE_TEMP_BOXGLOBAL_MAGICC_INDIVIDUAL_UPDATED.OUT'), header=17, delim_whitespace=True)

runs = pd.concat([runs_high,runs_low.iloc[:,1:]], axis=1, ignore_index=True)

runs.columns = ['YEARS'] + list(range(1200))

med = np.median(runs.iloc[:,1:].values, axis=1) + 0.61
p5 = np.percentile(runs.iloc[:,1:].values, 5, axis=1) + 0.61
p95 = np.percentile(runs.iloc[:,1:].values, 95, axis=1) + 0.61
years = runs.YEARS.values

scenarios['cat_current'] = pd.DataFrame()
adjustment['cat_current'] = str(sum(med[np.isin(years,list(range(2011,2021)))])/len(med[np.isin(years,list(range(2011,2021)))])-1.09)
scenarios['cat_current']['year'] = list(range(2000,2101))
scenarios['cat_current']['median'] = med[np.isin(years,scenarios['cat_current']['year'])]
scenarios['cat_current']['p5'] = p5[np.isin(years,scenarios['cat_current']['year'])]
scenarios['cat_current']['p95'] = p95[np.isin(years,scenarios['cat_current']['year'])]
scenarios['cat_current']['median'] = [str(float(w) - float(adjustment['cat_current'])) for w in scenarios['cat_current']['median']]
scenarios['cat_current']['p5'] = [str(float(w) - float(adjustment['cat_current'])) for w in scenarios['cat_current']['p5']]
scenarios['cat_current']['p95'] = [str(float(w) - float(adjustment['cat_current'])) for w in scenarios['cat_current']['p95']]
scenarios['cat_current']['wlvl median'] = [str(np.round(float(w),1)) for w in scenarios['cat_current']['median']]
scenarios['cat_current']['wlvl p5'] = [str(np.round(float(w),1)) for w in scenarios['cat_current']['p5']]
scenarios['cat_current']['wlvl p95'] = [str(np.round(float(w),1)) for w in scenarios['cat_current']['p95']]



'''
print(adjustment)

import matplotlib.pyplot as plt

for scenario in ['rcp26','rcp45','rcp60','rcp85','cat_current','h_cpol','d_delfrag','o_1p5c']:
    xpoints = scenarios['rcp26']['year']
    ypoints = [1.5 for w in scenarios[scenario]['median']]
    plt.plot(xpoints, ypoints, label = '1.5 degrees')
    xpoints = scenarios[scenario]['year']
    ypoints = [float(w)-float(adjustment[scenario]) for w in scenarios[scenario]['median']]
    plt.plot(xpoints, ypoints, label='Adjusted')
    xpoints = scenarios[scenario]['year']
    ypoints = [float(w)  for w in scenarios[scenario]['median']]
    plt.plot(xpoints, ypoints, label='Original')
    plt.title(scenario)
    plt.xlabel('Year')
    plt.ylabel('Warming Level')
    plt.legend()
    print(scenario)
    print(1.09+float(adjustment[scenario]))
    #print('1.1 reached for '+scenario+' in:')
    #print(2000 + len([float(w)-float(adjustment[scenario]) for w in scenarios[scenario]['median'] if round(float(w),1) < 1.1 ]))
    #print('For Scenario '+scenario+' the Warming in 2010 to 2019 is:' )
    #scenarioDF = scenarios[scenario]
    #print(sum([float(i) for i in scenarioDF.loc[scenarioDF['year'].isin(list(range(2011,2021))), 'median'].values.flatten()]) / 10)
    plt.show()
'''


