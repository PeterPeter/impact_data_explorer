import os, pickle

import pandas as pd
import json
import numpy as np
import xarray as xr

import pycountry

possible_paths = [
    '/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/',
    '/home/tooli/impact_data_explorer/data/',
    '/Users/niklasschwind/Documents/impact_data_explorer/data/'
]


for data_path in possible_paths:
    if os.path.isdir(data_path):
        break
data = pd.read_csv('../meta/impact_data_explorer_variables.csv', header=1, sep=';')
data = data.applymap(lambda x: x.strip() if isinstance(x, str) else x)
data = data.fillna('-')


meta = {}
meta['units'] = []
meta['units'].append({'name' : 'percentage points', 'short' : 'pp', 'id' : 'percentagepoints'})
used_units = []
meta['vars'] = []
excludeVars = ['thawdepth', 'led', 'ped', 'lef', 'pef']
includeVars = ['ec4','ec2']
for i in data.index:
    var = data.iloc[i]['Abbreviation']
    show = data.iloc[i]['Switch: show_or _not'] == 1
    if isinstance(var, str):
        if (os.path.isdir(data_path +'/wlvl_diff/'+var+'/') or var in includeVars) and var not in excludeVars and show:
            short = data.iloc[i]['Unit']
            id_ = short.replace(' ','_').replace('%','percent').replace('°C','degC')
            longname = data.iloc[i]['Unit longname']
            if data.iloc[i]['display relative or absolute changes?'] == 'relative' :
                id_unit = 'percent'

                if id_unit not in used_units:
                    meta['units'].append(
                        {
                            'name' : 'relative change in percent',
                            'short' : '%',
                            'id' : id_unit,
                        }
                    )
                    used_units.append(id_unit)
            else:
                id_unit = id_

            id_unit_orig = id_
            if id_unit_orig not in used_units:
                meta['units'].append(
                    {
                        'name' : longname,
                        'short' : short.replace('-','\u207B').replace('1','\u00B9').replace('2','\u00B2'),
                        'id' : id_unit_orig,
                    }
                )
                used_units.append(id_unit_orig)

            tmp = {
                'id' : var,
                'name' : data.iloc[i]['Variable - new name'],
                'source' : data.iloc[i]['Data Source'],
                'group' : data.iloc[i]['group 1'],
                'spatial_weighting' : data.iloc[i]['spatial aggregation choice'].replace(' ','').split(','),
                'unit' : id_unit,
                'orig_unit' : id_unit_orig,
                'scale_type' : data.iloc[i]['scale_type'].lower().replace(' ','_').replace('/','_'),
                'scale_direction' : int(data.iloc[i]['scale_direction']),
                'description' : data.iloc[i]['Description'],
                'disclaimer' : data.iloc[i]['Assumptions & Limitations'],
                'reference_period' : data.iloc[i]['Reference period'],
            }
            #change unit of certain indicators to percentage points
            if tmp['id'] in ['lew', 'fldfrc', 'lec', 'leh', 'pew', 'pec', 'per', 'peh', 'hursAdjust']:
                tmp['unit'] = 'percentagepoints'

            if  data.iloc[i]['display relative or absolute changes?'] in ['-','null']:
                tmp['change_type'] = ''
            else:
                tmp['change_type'] = data.iloc[i]['display relative or absolute changes?']
            if tmp['id'] == 'ec1':
                tmp['change_type'] = 'relative'
                tmp['unit'] = 'percentagepoints'
            if data.iloc[i]['Temporal Resolution'] in ['yearly','per growing season']:
                tmp['temporal_averaging'] = ['annual']
            else:
                tmp['temporal_averaging'] = ['annual', 'MAM', 'JJA', 'SON', 'DJF']

            meta['vars'].append(tmp)

meta['temporal_averagings'] = [
    { 'id': 'annual', 'name': 'Annual' }, 
    { 'id': 'MAM', 'name': 'Mar, April, May' }, 
    { 'id': 'JJA', 'name': 'June, July, August' }, 
    { 'id': 'SON', 'name': 'September, October, November' }, 
    { 'id': 'DJF', 'name': 'December, January, February' },
]

meta['spatial_weightings'] = [
    { 'id': 'area', 'name': 'Area-weighted average' }, 
    { 'id': 'pop', 'name': 'Population-weighted average' }, 
    { 'id': 'gdp', 'name': 'GDP-weighted average' }, 
    { 'id': 'other', 'name': 'Sum' } 
]


meta['variable_groups'] = []
groups = np.unique([var for var in data['group 1'] if isinstance(var, str)])
for group in groups:
    tmp = {
      "id": group,
      "name": group.capitalize(),
      "children": [v for v in data.loc[(data['group 1']==group), 'Abbreviation'].values if v in [i['id'] for i in meta['vars']]],
      "type" :  [v for v in data.loc[(data['group 1']==group), 'group 3']][0],
    }
    meta['variable_groups'].append(tmp)

cou_bounds = {}
meta['countries'] = []
for cou in pycountry.countries:
    iso = cou.alpha_3
    if os.path.isfile(data_path + 'topojsons/'+iso+'_0_topo.json'):
        mask = xr.open_dataset(data_path+'../data/masks/'+iso+'/masks/'+iso+'_360x720lat89p75to-89p75lon-179p75to179p75_latWeight.nc4')
        lons = mask.lon.values
        if np.min(lons) < -170 and np.max(lons) > 170:
            lons[lons<0] += 360
        cou_bounds[iso] = [[float(lons.min()),float(mask.lat.values.max())], [float(lons.max()),float(mask.lat.values.min())]]
        meta['countries'].append(
            {
                'id' : iso,
                'name' : cou.name,
            }
        )

for continents in ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA']:
    mask = xr.open_dataset(data_path+'../data/masks/'+continents+'/masks/'+continents+'_360x720lat89p75to-89p75lon-179p75to179p75_latWeight.nc4')
    lons = mask.lon.values
    if np.min(lons) < -170 and np.max(lons) > 170:
        lons[lons<0] += 360
    cou_bounds[continents] = [[float(lons.min()),float(mask.lat.values.max())], [float(lons.max()),float(mask.lat.values.min())]]
    meta['countries'].append(
        {
            'id' : continents,
            'name' : continents.replace('_',' ').capitalize(),
        }
    )

cou_bounds['FRA'] = [[-6.5,51.6],[11,40.5]]
cou_bounds['USA'] = [[-173,71],[-60,16]]
cou_bounds['RUS'] = [[20.4,82.2],[193.7,31.1]]
cou_bounds['NZL'] = [[163.1,-32.8],[186.7,-49.7]]
cou_bounds['GEO'] = [[38.9, 43.5],[50.9, 40.7]]
cou_bounds['GRL'] = [[-442.3, 84.1],[-362.8, 58.1]]
cou_bounds['CAN'] = [[-515.4, 83.9],[-406.4, 34.9]]
cou_bounds['USA'] = [[-173,71],[-60,16]]

cou_bounds['RUS'] = [[20.4,82.2],[193.7,31.1]]
cou_bounds['NZL'] = [[163.1,-32.8],[186.7,-49.7]]
cou_bounds['GEO'] = [[38.9, 43.5],[50.9, 40.7]]
cou_bounds['GRL'] = [[-442.3, 84.1],[-362.8, 58.1]]
cou_bounds['CAN'] = [[-515.4, 83.9],[-406.4, 34.9]]
cou_bounds['AIA'] = [[-63.3, 18.4],[-62.8, 18]]
cou_bounds['CUW'] = [[-69.2, 12.4],[-68.6, 11.9]]
cou_bounds['MSR'] = [[-62.3, 16.8],[-62.1, 16.7]]
cou_bounds['BLM'] = [[-62.9, 18],[-62.7, 17.8]]
cou_bounds['KNA'] = [[-62.9, 17.4],[-62.5, 17]]
cou_bounds['MAF'] = [[-63.1, 18.1],[-62.9, 18]]
cou_bounds['TCA'] = [[-72.5, 21.9],[-71, 21.2]]
cou_bounds['SYC'] = [[55.1, -4.2],[56, -4.8]]
cou_bounds['FJI'] = [[175.8, -15.2],[-178, -20]]
cou_bounds['KIR'] = [[-161.1, 4.9],[-156.6, 1.4]]
cou_bounds['NRU'] = [[166.8, -0.4],[167, -0.5]]
cou_bounds['BMU'] = [[-64.9, 32.4],[-64.6, 32.2]]
cou_bounds['IMN'] = [[-4.8, 54.4],[-4.2, 54]]
cou_bounds['COK'] = [[-160.1, -18.7],[-157.3, -22]]
cou_bounds['SMR'] = [[12.4, 43.9],[12.5, 43.8]]
cou_bounds['SHN'] = [[-5.8, -15.8],[-5.5, -16]]
cou_bounds['LIE'] = [[9.4, 74.2],[9.6, 47]]
cou_bounds['MCO'] = [[7.4, 43.7],[7.44, 43.7]]
cou_bounds['Asia'] = [[30.39, 72.4],[172.6, 6.5]]
cou_bounds['Oceania'] = [[93.9, 5.6],[179.1, -49.3]]
cou_bounds['South America'] = [[-86.9, 10.6],[-24.1, -56.9]]
cou_bounds['Europe'] = [[-22.8, 69.3],[47.8, 33.7]]


#meta['cou_bounds'] = cou_bounds

# check which countries are large
for cou in meta['countries']:
    iso = cou['id']
    fl = data_path + '../data/masks/%s/masks/%s_360x720lat89p75to-89p75lon-179p75to179p75_latWeight.nc4' %(iso,iso)
    if os.path.isfile(fl):
        mask = xr.open_dataset(fl)[iso]
        if np.sum(mask.values>0) > 1000:
            cou['large'] = True
            print(iso, mask.shape[0]*mask.shape[1], np.sum(mask.values>0))
        else:
            cou['large'] = False
    else:
        print('mask missing: '+iso)

meta['countries'].append(
    {
        'id' : 'GLOBAL',
        'name' : 'Global',
        'large' : True,
    }
)

the_dict = pickle.load(open('../meta/country_groups.pkl', 'rb'))
meta['country_groups'] = []
for name,country_group in the_dict.items():
    tmp = {
      "id": name.lower().replace(' ',''),
      "name": name,
      "children": country_group
    }
    meta['country_groups'].append(tmp)


exclude_countries = {}
excludes = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA''AIA','ASM','ATG','AUS','BGD','BHS','BLR','BLZ','BMU','BRA','BRB','CAN','CHN','COK','COL','COM','CPV','CRI','CUB','CYM','DEU','DMA','DOM','ESP','FJI','FRA','FSM','GBR','GRD','GTM','GUM','HKG','HND','HTI','IND','IRL','JAM','JPN','KHM','KNA','KOR','LAO','LCA','LKA','MAC','MAF','MAR','MDG','MEX','MMR','MOZ','MSR','MUS','MYS','NCL','NIC','NIU','NOR','OMN','PAK','PAN','PER','PHL','PNG','PRI','PRK','PYF','RUS','SLB','SLV','SOM','SWZ','SXM','SYC','TCA','THA','TJK','TON','TTO','TWN','UKR','USA','VCT','VEN','VGB','VIR','VNM','VUT','WSM','YEM','ZAF','ZWE']
exclude_countries['ec3'] = [c['id'] for c in meta['countries'] if c['id'] not in excludes]


exclude_countries['ec2'] = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA']
exclude_countries['ec3'] = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA']
exclude_countries['ec4'] = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA']

#meta['exclude_countries'] = {}

#excludes = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA''AIA','ASM','ATG','AUS','BGD','BHS','BLR','BLZ','BMU','BRA','BRB','CAN','CHN','COK','COL','COM','CPV','CRI','CUB','CYM','DEU','DMA','DOM','ESP','FJI','FRA','FSM','GBR','GRD','GTM','GUM','HKG','HND','HTI','IND','IRL','JAM','JPN','KHM','KNA','KOR','LAO','LCA','LKA','MAC','MAF','MAR','MDG','MEX','MMR','MOZ','MSR','MUS','MYS','NCL','NIC','NIU','NOR','OMN','PAK','PAN','PER','PHL','PNG','PRI','PRK','PYF','RUS','SLB','SLV','SOM','SWZ','SXM','SYC','TCA','THA','TJK','TON','TTO','TWN','UKR','USA','VCT','VEN','VGB','VIR','VNM','VUT','WSM','YEM','ZAF','ZWE']
#meta['exclude_countries']['ec3'] = [c['id'] for c in meta['countries'] if c['id'] not in excludes]

#meta['exclude_countries']['ec2'] = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA']
#meta['exclude_countries']['ec3'] = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA']
#meta['exclude_countries']['ec4'] = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA']


exclude_vars_for_maps = ['ec2', 'ec2_720x360']

'''Test'''









