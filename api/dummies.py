

dummy_empty_map = {
  "agreement": None,
  "attrs": {}, 
  "coords": {
    "ID": {
      "attrs": {}, 
      "data": "HadGEM2-ES_rcp60", 
      "dims": []
    }, 
    "lat": {
      "attrs": {
        "axis": "Y", 
        "long_name": "latitude", 
        "standard_name": "latitude", 
        "units": "degrees_north"
      }, 
      "data": [None,None], 
      "dims": [
        "lat"
      ]
    }, 
    "lon": {
      "attrs": {
        "axis": "X", 
        "long_name": "longitude", 
        "standard_name": "longitude", 
        "units": "degrees_east"
      }, 
      "data": [None, None, None,], 
      "dims": [
        "lon"
      ]
    }, 
    "region": {
      "attrs": {}, 
      "data": "BEN", 
      "dims": []
    }
  }, 
  "data": None, 
  "dims": [
    "lat", 
    "lon"
  ], 
  "name": "var"}


dummy_empty_ts = {
			"lower": None, 
			"median": None, 
			"upper": None, 
			"wlvl": None, 
			"year": None, 
			}

new_dummy_ts = {
  'status': {
    'type': 'error',
    'message': "*Something went wrong.* \nPlease try to refresh the page. If the problem persists, send us a short message indicating which indicator and country or province you selected."
  }
}

Exeeded_Median_dummy_ts = {
  'status': {
    'type': 'disclaimer',
    'message': "*We chose not to display this graph.* \nProjected changes attain very high values which hint at challenges with the underlying data for this indicator and country or province."
  }
}

disclaimer_dummy_ts = {
  'status': {
    'type': 'disclaimer',
    'message': "*Something went wrong.*  \nWe are doing our best to fix it as soon as possible."
  }
}

new_dummy_maps = {
  'status': {
    'type': 'error',
    'message': "*Something went wrong.* \nPlease try to refresh the page. If the problem persists, send us a short message indicating which indicator and country or province you selected."
  }
}

excluded_indicator_dummy_maps = {
  'status': {
    'type': 'disclaimer',
    'message': "This indicator was exluded due to unsufficient model agreement"
  }
}

color_range_error_floods = {
  'status': {
    'type': 'disclaimer',
    'message': "*We chose not to display this map.* \nYou selected an indicator describing damages from flood events happening at a frequency equal or close to zero over many model grid cells in the reference period. As a result, projected relative changes attain extremely high values over some of these grid cells as soon as they start occurring due to global warming. We therefore chose not to display the spatially explicit data."
  }
}
color_range_error = {
  'status': {
    'type': 'disclaimer',
    'message': "*We chose not to display this map.* \nYou selected an indicator describing damages happening at a frequency equal or close to zero over many model grid cells in the reference period. As a result, projected relative changes attain extremely high values over some grid cells as soon as they start occurring due to global warming. We therefore chose not to display the spatially explicit data."
  }
}

tropical_cyone_map_error = {
  'status': {
    'type': 'disclaimer',
    'message': "*We chose not to display this map.* \nYou selected an indicator describing damages from tropical cyclones happening at a frequency equal or close to zero over many model grid cells in the reference period. As a result, projected relative changes attain extremely high values over some grid cells as soon as they start occurring due to global warming."
  }
}

not_available_on_regional_level_error = {
  'status': {
    'type': 'disclaimer',
    'message': "*Data not avialable.* \nThe Time Series are unfortunately not available at the province level for this indicator."
  }
}