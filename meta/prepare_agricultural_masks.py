import os,sys,glob,time,collections,gc,pickle
import numpy as np
import pandas as pd
import xarray as xr
import xesmf as xe

# for plotting
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import cartopy.crs as ccrs

from functools import partial
import pyproj

from shapely.geometry import Polygon
from shapely.ops import transform

project = partial(
    pyproj.transform,
    pyproj.Proj(init='epsg:4326'), # source coordinate system
    pyproj.Proj(init='epsg:26913'))

for crop, crop_name in {'MAIZ':'maize', 'RICE':'rice', 'WHEA':'wheat', 'SOYB':'soy'}.items():
    rain_ = xr.open_rasterio('/Users/peterpfleiderer/Projects/data/agriculture_masks/dataverse_files/spam2010v2r0_global_phys_area.geotiff/spam2010V2r0_global_A_'+crop+'_R.tif')
    irr_ = xr.open_rasterio('/Users/peterpfleiderer/Projects/data/agriculture_masks/dataverse_files/spam2010v2r0_global_phys_area.geotiff/spam2010V2r0_global_A_'+crop+'_I.tif')
    all_ = xr.open_rasterio('/Users/peterpfleiderer/Projects/data/agriculture_masks/dataverse_files/spam2010v2r0_global_phys_area.geotiff/spam2010V2r0_global_A_'+crop+'_A.tif')

    rain_ = rain_.rename({'x':'lon', 'y':'lat'}).squeeze()
    rain_.values[rain_ == -1] = np.nan
    irr_ = irr_.rename({'x':'lon', 'y':'lat'}).squeeze()
    irr_.values[irr_ == -1] = np.nan
    all_ = all_.rename({'x':'lon', 'y':'lat'}).squeeze()
    all_.values[all_ == -1] = np.nan

    dlat = np.abs(np.diff(all_.lat).mean())
    lat_area = np.array([transform(project, Polygon([(all_.lon[0],y+0.5*dlat),(all_.lon[0],y-0.5*dlat),(all_.lon[1],y-0.5*dlat),(all_.lon[1],y+0.5*dlat)])).area / 10**4 for y in data.lat])
    area_ = np.repeat(lat_area[np.newaxis,:], len(all_.lon), 0).T

    out = {'rain':rain_ / area_, 'irr':irr_ / area_, 'all':all_ / area_}
    xr.Dataset(out).to_netcdf('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/agriculture_masks/physical_area_'+crop_name+'.nc')

    # plt.close('all')
    # fig,axes = plt.subplots(nrows=3, ncols=1, figsize=(10,10), subplot_kw={'projection': ccrs.PlateCarree()})       
    # for ax,key in zip(axes,out.keys()):
    #     ax.coastlines()
    #     ax.set_title(''+crop_name+' '+key)
    #     im = ax.contourf(all_.lon, all_.lat, out_regrid[key], cmap='jet', levels=np.linspace(0,1,101))

    # fig.colorbar(im, ax=axes, shrink=0.6, label='fraction of the grid-cell area')
    # plt.savefig('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/agriculture_masks/phsyical_'+crop_name+'.png', transparent=False, dpi=100, bbox_inches='tight'); plt.close()
    # gc.collect()

    refrence_grid = xr.Dataset({'lat': (['lat'], np.arange(-89.75,90.25,0.5).round(2)),'lon': (['lon'], np.arange(-179.75,180.25,0.5).round(2)),})
    regridder = xe.Regridder(irr_ / area_, refrence_grid, 'bilinear', reuse_weights=True)
    out_regrid = {key:regridder(var) for key,var in out.items()}

    xr.Dataset(out_regrid).to_netcdf('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/agriculture_masks/physical_area_'+crop_name+'_720x360.nc')


    plt.close('all')
    fig,axes = plt.subplots(nrows=3, ncols=1, figsize=(10,10), subplot_kw={'projection': ccrs.PlateCarree()})

    for ax,key in zip(axes,out.keys()):
        ax.coastlines()
        ax.set_title(''+crop_name+' '+key)
        im = ax.contourf(out_regrid[key].lon, out_regrid[key].lat, out_regrid[key], cmap='jet', levels=np.linspace(0,1,101))

    fig.colorbar(im, ax=axes, shrink=0.6, label='fraction of the grid-cell area')
    plt.savefig('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/agriculture_masks/phsyical_'+crop_name+'_720x360.png', transparent=False, dpi=100, bbox_inches='tight'); plt.close()
    gc.collect()