import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Near-Surface Wind Speed',
	 'orig_name': 'sfcWind',
	 'short_name': 'sfcWindAdjust',
	 'rel-abs': 'relative',
	 'source': 'ISIMIP',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': np.mean,
	 'monthly_aggreagation': np.mean,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/sfcWindAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/sfcWind_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/sfcWind_*'}
