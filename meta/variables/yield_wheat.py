import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Yields (wheat)',
	 'orig_name': 'yield_wheat',
	 'short_name': 'yield_wheat',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'per growing season',
	 'seasonal_aggreagation': 'sum',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/yield_wheat/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_yield_wheat_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_yield_wheat_*'}