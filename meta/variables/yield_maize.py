import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Yields (maize)',
	 'orig_name': 'yield_maize',
	 'short_name': 'yield_maize',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'per growing season',
	 'seasonal_aggreagation': 'sum',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/yield_maize/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/agriculture/*/*/historical/*_yield-mai*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/agriculture/*/*/*/*_yield_maize_*'}