import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Near-Surface Air Temperature',
	 'orig_name': 'tas',
	 'short_name': 'tasAdjust',
	 'rel-abs': 'absolute',
	 'source': 'ISIMIP',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': np.mean,
	 'monthly_aggreagation': np.mean,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/tasAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/tas_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/tas_*'}
