import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Snow Depth',
	 'orig_name': 'snd',
	 'short_name': 'snd',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'monthly',
	 'seasonal_aggreagation': 'sum ??',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/snd/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/snd_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/snd_*'}