import glob
import numpy as np
details = {'overwrite': 'no',
    'long_name': 'Population exposed to floods',
    'orig_name': 'per',
    'short_name': 'pef',
    'source': 'ISIMIP - Secondary Output',
    'rel-abs': 'absolute',
	'correction_factor' : 100.0,
    'temporal_resolution': 'yearly',
    'seasonal_aggreagation': 'nan',
    'excludeImpModels' : ['MPI-HM'],
    'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/pef/*/*/*/*',
    'in_files': 
        glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/future/*rcp*per*') + \
        glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/historical/*historical*per*')
    }