import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Heat Wave Magnitude Index daily',
	 'orig_name': 'hwmid',
	 'short_name': 'hwmid',
	 'rel-abs': 'relative',
	 'source': 'ISIMIP',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': np.nanmax,
	 'monthly_aggreagation': np.nanmax,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/hwmid/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/slange/counting_extremes_slim/data/in/heatwavedarea/hwmid/calc/*hist*31.nc4',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/slange/counting_extremes_slim/data/in/heatwavedarea/hwmid/calc/*31.nc4'}
