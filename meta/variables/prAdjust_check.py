import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Precipitation (Rainfall + Snowfall)',
	 'orig_name': 'pr',
	 'short_name': 'prAdjust',
	 'rel-abs': 'relative',
	 'source': 'ISIMIP',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': np.sum,
	 'monthly_aggreagation': np.sum,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/prAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/pr_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/pr_*'}
