import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Surface Runoff',
	 'orig_name': 'qs',
	 'short_name': 'qs',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'monthly',
	 'seasonal_aggreagation': 'np.mean',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/qs/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_qs_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_qs_*'}