import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Surface Downwelling Shortwave Radiation',
	 'orig_name': 'rsds',
	 'short_name': 'rsdsAdjust',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': np.mean,
	 'monthly_aggreagation': np.mean,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/rsdsAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/rsds_day_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/rsds_day_*'}