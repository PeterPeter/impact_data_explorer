import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Precipitation (Rainfall + Snowfall)',
	 'orig_name': 'prAdjust',
	 'short_name': 'prAdjust',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': 'sum',
	 'monthly_aggreagation': 'sum',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/prAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/prAdjust_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/prAdjust_*'}