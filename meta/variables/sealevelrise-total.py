import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Total Sea-Level Rise',
	 'orig_name': 'sealevelrise-total',
	 'short_name': 'sealevelrise-total',
	 'source': 'ISIMIP',
	 'rel-abs': 'absolute',
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': 'np.mean',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/sealevelrise-total/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_sealevelrise-total_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_sealevelrise-total_*'}