import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Population exposed to heatwave',
	 'orig_name': 'peh',
	 'short_name': 'peh',
	 'source': 'ISIMIP - Secondary Output',
	 'rel-abs': 'absolute',
	 'correction_factor' : 100.0,
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': 'nan',
    'excludeImpModels' : ['MPI-HM'],
    'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/peh/*/*/*/*',
    'in_files': 
        glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/future/*rcp*peh*') + \
        glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/historical/*historical*peh*')
    }