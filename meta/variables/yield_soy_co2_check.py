import glob, operator
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Yields (soy)',
	 'orig_name': 'yield-soy',
	 'short_name': 'yield_soy_co2',
	 'mask_file' : '/p/tmp/pepflei/impact_data_explorer_data/agriculture_masks/physical_area_soy_720x360.nc',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/yield_soy_co2/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/agriculture/*/*/historical/*historical*_co2_yield-soy*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/agriculture/*/*/future/*rcp*_co2_yield-soy*',
	 'irrealistic_values' : [{'operator' : operator.gt, 'threshold' : 50.}, {'operator' : operator.lt, 'threshold' : -50.}],	 
	 }