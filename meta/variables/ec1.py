import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Heat stress impact on labour Productivity',
	 'orig_name': 'ec1',
	 'short_name': 'ec1',
	 'source': 'ISIMIP - Secondary Output',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': 'nan',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/ec1/*/*/*/*',
	 'in_files' : glob.glob('/p/tmp/vmaanen/new/Labour_Productivity/indoor/*')
	 }