import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Annual Maximum Thaw Depth',
	 'orig_name': 'thawdepth',
	 'short_name': 'thawdepth',
	 'source': 'ISIMIP',
	 'rel-abs': 'absolute or relative?',
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': 'np.mean',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/thawdepth/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_thawdepth_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_thawdepth_*'}