import glob, operator
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Surface Runoff',
	 'orig_name': 'qs',
	 'short_name': 'qs',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'mixed',
	 'monthly_aggreagation': np.mean,
	 'seasonal_aggreagation': np.mean,
	 'excludeImpModels' : ['MPI-HM','ORCHIDEE-DGVM'],
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/qs/*/*/*/*',
	 'in_files': 
	 		glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/*/*/*/*/*historical*histsoc_co2*_qs_*') + \
			glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/CLM45/*/*/*historical*2005soc_co2*_qs_*') + \
			glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/*/*/*/future/*rcp*2005soc_co2*_qs_*'),
	 'ref_mask' : {
		'var' : 'qs',
		'threshold' : 0.05 / (60 * 60 * 24),
		'operator' : operator.lt,
	}
}
