import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Minimum of Daily Discharge',
	 'orig_name': 'mindis',
	 'short_name': 'mindis',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'monthly',
	 'seasonal_aggreagation': 'min ?',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/mindis/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_mindis_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_mindis_*'}