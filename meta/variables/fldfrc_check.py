import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Flood Fraction',
	 'orig_name': 'fldfrc',
	 'short_name': 'fldfrc',
	 'source': 'ISIMIP - Extreme Events',
	 'correction_factor' : 100.0,
	 'rel-abs': 'absolute',
	 'temporal_resolution': 'yearly',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/fldfrc/*/*/*/*',
	 'in_files' : glob.glob('/p/projects/isimip/isimip/inga/ngfs/flood_data/*/fldfrc*')
	 }
