import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Flood Depth',
	 'orig_name': '(flddph)',
	 'short_name': '(flddph)',
	 'source': 'ISIMIP - Extreme Events',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': nan}