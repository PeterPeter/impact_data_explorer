import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Discharge',
	 'orig_name': 'dis',
	 'short_name': 'dis',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': 'sum ??',
	 'monthly_aggreagation': 'sum ??',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/dis/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_dis_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_dis_*'}