import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Population exposed to drought',
	 'orig_name': 'ped',
	 'short_name': 'ped',
	 'source': 'ISIMIP - Secondary Output',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': nan,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/ped/*/*/*/*'}