import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Near-Surface Specific Humidity',
	 'orig_name': 'huss',
	 'short_name': 'hussAdjust',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': np.mean,
	 'monthly_aggreagation': np.mean,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/hussAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/huss_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/huss_*'}