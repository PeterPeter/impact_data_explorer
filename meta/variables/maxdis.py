import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Maximum of Daily Discharge',
	 'orig_name': 'maxdis',
	 'short_name': 'maxdis',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'monthly',
	 'seasonal_aggreagation': 'max ?',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/maxdis/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_maxdis_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_maxdis_*'}