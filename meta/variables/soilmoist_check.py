import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Total Soil Moisture Content',
	 'orig_name': 'soilmoist',
	 'short_name': 'soilmoist',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'monthly',
	 'seasonal_aggreagation': np.mean,
	 'excludeImpModels' : ['DBH', 'JULES-ES-55', 'MPI-HM', 'ORCHIDEE-DGVM', 'SWAT-VUB', 'VEGAS', 'MATSIRO','LPJ-GUESS'],
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/soilmoist/*/*/*/*',

	 'in_files': 
	 		glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/*/*/*/*/*historical*histsoc_co2*_*moist_*') + \
			glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/*/*/*/*/web*historical*soc_co2*rootmoist*_*') + \
			glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/CLM45/*/*/*historical*2005soc_co2*_*moist_*') + \
			glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/*/*/*/future/*rcp*2005soc_co2*_*moist_*')}

layer_dict = {
	'CLM45' : range(7),
	'JULES-W1' : range(3),
	'LPJmL' : range(3),
	'ORCHIDEE' : range(9)
}