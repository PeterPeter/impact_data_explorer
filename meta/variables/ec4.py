import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': '1 in 100 Year Expected Damages - Tropical Cyclones',
	 'orig_name': 'ec4',
	 'short_name': 'ec4',
	 'source': 'CLIMADA',
	 'rel-abs': nan,
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': nan}