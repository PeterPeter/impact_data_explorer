import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Population exposed to drought',
	'orig_name': 'ped',
	'short_name': 'ped',
	'source': 'ISIMIP - Secondary Output',
	'rel-abs': 'absolute',
	'correction_factor' : 100.0,
	'temporal_resolution': 'yearly',
	'excludeImpModels' : ['MPI-HM'],
	'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/ped/*/*/*/*',
	'in_files': 
		glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/future/*rcp*ped*') + \
		glob.glob('/p/projects/isimip/isimip/ISIMIP2b/DerivedOutputData/Lange2020/*/*/historical/*historical*ped*')
	}

