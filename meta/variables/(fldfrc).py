import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Flood Fraction',
	 'orig_name': '(fldfrc)',
	 'short_name': '(fldfrc)',
	 'source': 'ISIMIP - Extreme Events',
	 'rel-abs': 'absolute (in %)',
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': nan}