import glob, operator
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Yields (rice)',
	 'orig_name': 'yield-ric',
	 'short_name': 'yield_rice_co2',
	 'mask_file' : '/p/tmp/pepflei/impact_data_explorer_data/agriculture_masks/physical_area_rice_720x360.nc',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'yearly',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/yield_rice_co2/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/agriculture/*/*/historical/*historical*_co2_yield-ric*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/agriculture/*/*/future/*rcp*_co2_yield-ric*',
	 'irrealistic_values' : [{'operator' : operator.gt, 'threshold' : 50.}, {'operator' : operator.lt, 'threshold' : -50.}],	 
	 }