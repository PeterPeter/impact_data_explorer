import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Annual Expected Damages - Tropical Cyclones',
	 'orig_name': 'ec3',
	 'short_name': 'ec3',
	 'source': 'CLIMADA',
	 'rel-abs': nan,
	 'temporal_resolution': 'yearly',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/ec3/*/*/*/*',
	 'seasonal_aggreagation': nan}
