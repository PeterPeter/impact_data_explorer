import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Daily Minimum Near-Surface Air Temperature',
	 'orig_name': 'tasminAdjust',
	 'short_name': 'tasminAdjust',
	 'source': 'ISIMIP',
	 'rel-abs': 'absolute',
	 'temporal_resolution': 'daily',
	 'seasonal_aggreagation': 'np.min',
	 'monthly_aggreagation': 'np.min',
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/tasminAdjust/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/tasminAdjust_*',
	 'in_files_pattern_future': '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/tasminAdjust_*'}