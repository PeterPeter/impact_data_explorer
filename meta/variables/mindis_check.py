import glob, operator
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Monthly Minimum of Daily Discharge',
	 'orig_name' : 'dis',
	 'short_name' : 'mindis',
	 'rel-abs': 'relative',
	 'source': 'ISIMIP',
	 'temporal_resolution': 'daily',
	 'monthly_aggreagation': np.min,
	 'seasonal_aggreagation': np.min,
	 'exlcudeImpModels' : ['MPI-HM'],
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/mindis/*/*/*/*',
	 'in_files_pattern_historical': '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*histsoc_co2*_dis_*daily*',
	 'in_files': glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*histsoc_co2*_dis_*daily*') \
	 			+ glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*2005soc_co2*_dis_*daily*') \
	 			+ glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/future/orchidee_*rcp*nosoc_co2*_dis_*daily*'),
	 'ref_mask' : {
		'var' : 'qs',
		'threshold' : 0.05 / (60 * 60 * 24),
		'operator' : operator.lt,
	}
}

#
