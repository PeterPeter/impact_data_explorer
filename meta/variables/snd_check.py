import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Snow Depth',
	 'orig_name': 'snd',
	 'short_name': 'snd',
	 'source': 'ISIMIP',
	 'rel-abs': 'relative',
	 'temporal_resolution': 'monthly',
	 'seasonal_aggreagation': np.mean,
	 'exlcudeImpModels' : ['JULES-ES-55', 'ORCHIDEE-DGVM', 'VEGAS'],
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/snd/*/*/*/*',
	 'in_files': glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/biomes/*/*/*/*historical*histsoc_co2*_snd_*') + glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/biomes/*/*/*/*historical*2005soc_co2*_snd_*') + glob.glob('/p/projects/isimip/isimip/ISIMIP2b/OutputData/biomes/*/*/future/*rcp*2005soc_co2*_snd_*')}

layer_dict = {
	'ORCHIDEE' : range(3)
}