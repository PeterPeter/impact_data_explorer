import glob
import numpy as np
details = {'overwrite': 'no',
	 'long_name': 'Land area exposed to wildfire',
	 'orig_name': 'lew',
	 'short_name': 'lew',
	 'source': 'ISIMIP - Secondary Output',
	 'rel-abs': 'percentage of land area (%)',
	 'temporal_resolution': 'yearly',
	 'seasonal_aggreagation': nan,
	 'wlvl_files_pattern': '/p/tmp/pepflei/impact_data_explorer_data/raw/lew/*/*/*/*'}