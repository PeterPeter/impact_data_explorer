import os,sys, importlib, glob,gc
import numpy as np
import topojson as tp
import geopandas
import pandas as pd

from geojson_rewind import rewind

import cartopy.io.shapereader as shapereader

cous = geopandas.read_file('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp')[['adm0_a3','geometry']]
for iso in cous['adm0_a3']:
    print(iso)
    out_file = '/p/tmp/pepflei/impact_data_explorer_data/topojsons/'+iso+'_0_topo.json'
    tmp = cous.loc[cous.adm0_a3==iso]
    topo = rewind(tp.Topology(tmp, prequantize=False).to_json())
    open(out_file, 'w').write(topo)

for iso in ['ESH','SSD','PSE']:
    shape = next(shapereader.Reader('/p/tmp/pepflei/impact_data_explorer_data/masks/'+iso+'/'+iso+'_adm_shp/gadm36_'+iso+'_0.shp').records()).geometry

    out_file = '/p/tmp/pepflei/impact_data_explorer_data/topojsons/'+iso+'_0_topo.json'    
    topo = rewind(tp.Topology(shape, prequantize=False).to_json())
    open(out_file, 'w').write(topo)



# import cartopy.io.shapereader as shapereader
# cous = shapereader.Reader('/p/projects/tumble/carls/shared_folder/data/shapefiles/world/ne_50m_admin_0_countries.shp').records()

# for cou in cous:
#     iso = cou.attributes['adm0_a3']
#     print(iso)
#     shape = cou.geometry
#     topo = rewind(tp.Topology(shape, prequantize=False).to_json())
#     open(out_file, 'w').write(topo)
#     # if iso == 'CAN':
#     #     asdas


#
