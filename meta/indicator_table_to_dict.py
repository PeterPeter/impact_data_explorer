import glob, json

import pandas as pd
import json
import numpy as np

data = pd.read_table('meta/impact_data_explorer_variables.csv', header=5, sep=';')
data = data.applymap(lambda x: x.strip() if isinstance(x, str) else x)

out = {}
for i in data.index:
    short_name = data.iloc[i]['Abbreviation']
    if isinstance(short_name, str):
        out[short_name] = {}
        out[short_name]['overwrite'] = 'no'
        out[short_name]['long_name'] = data.iloc[i]['Variable']
        out[short_name]['orig_name'] = short_name
        out[short_name]['short_name'] = short_name
        out[short_name]['source'] = data.iloc[i]['Data Source']

        out[short_name]['rel-abs'] = data.iloc[i]['display relative or absolute changes?']


        out[short_name]['temporal_resolution'] = data.iloc[i]['Temporal Resolution']

        tmp = data.iloc[i]['temporal aggregation for seasonal data']
        if tmp == 'min':
            out[short_name]['seasonal_aggreagation'] = 'np.min'
        elif tmp == 'mean':
            out[short_name]['seasonal_aggreagation'] = 'np.mean'
        elif tmp == 'max':
            out[short_name]['seasonal_aggreagation'] = 'np.max'
        else:
            out[short_name]['seasonal_aggreagation'] = tmp

        if out[short_name]['temporal_resolution'] == 'daily':
            out[short_name]['monthly_aggreagation'] = out[short_name]['seasonal_aggreagation']

        if out[short_name]['source'] in ['ISIMIP','ISIMIP - Secondary Output'] and data.iloc[i]['group 1'] != 'Climate':
            out[short_name]['wlvl_files_pattern'] = '/p/tmp/pepflei/impact_data_explorer_data/raw/'+short_name+'/*/*/*/*'
        if out[short_name]['source'] == 'ISIMIP' and data.iloc[i]['group 1'] == 'Climate':
            out[short_name]['wlvl_files_pattern'] = '/p/tmp/pepflei/impact_data_explorer_data/raw/'+short_name+'/*/*/*'

        if out[short_name]['source'] == 'ISIMIP' and data.iloc[i]['group 1'] != 'Climate':
            out[short_name]['in_files_pattern_historical'] = '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_'+short_name+'_*'
            out[short_name]['in_files_pattern_future'] = '/p/projects/isimip/isimip/ISIMIP2b/OutputData/water_global/*/*/*/*_'+short_name+'_*'
        if out[short_name]['source'] == 'ISIMIP' and data.iloc[i]['group 1'] == 'Climate':
            out[short_name]['in_files_pattern_historical'] = '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/'+short_name+'_*'
            out[short_name]['in_files_pattern_future'] = '/p/projects/isimip/isimip/ISIMIP2b/InputData/GCM_atmosphere/biascorrected/global/*/*/'+short_name+'_*'

        out_file = open('meta/variables/'+short_name+'.py', 'w')
        out_file.write('import glob\nimport numpy as np\n')
        out_file.write('details = '+str(out[short_name]).replace(',',',\n\t'))
        out_file.close()








#
