import pandas as pd
import json, pickle
import numpy as np
import cartopy.io.shapereader as shapereader


import pycountry


cous = shapereader.Reader('/Users/peterpfleiderer/Projects/data/shapefiles/world/ne_50m_admin_0_countries.shp')

country_groups = {}

# for continent in np.unique([info.attributes['continent'] for info in cous.records()]):
# 	country_groups[continent] = [info.attributes['adm0_a3'] for info in cous.records() if info.attributes['continent']==continent]

for subregion in np.unique([info.attributes['subregion'] for info in cous.records()]):
	country_groups[subregion] = [info.attributes['adm0_a3'] for info in cous.records() if info.attributes['subregion']==subregion]

country_groups['Northern Africa'] += ['ESH']
country_groups['Middle Africa'] += ['SSD']
country_groups['Western Asia'] += ['PSE']

country_groups['Continents'] = ['AFRICA','ASIA','EUROPE','NORTH_AMERICA','OCEANIA','SOUTH_AMERICA']

pickle.dump(country_groups, open('meta/country_groups.pkl', 'wb'))

#
