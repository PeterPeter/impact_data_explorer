import os,sys,glob,time,collections,gc,pickle
import numpy as np
from netCDF4 import Dataset,num2date
import matplotlib.pylab as plt
import pandas as pd

sys.path.append('/Users/peterpfleiderer/Projects/git-packages/wlcalculator/app/')
import wacalc.CmipData as CmipData; reload(CmipData)


models = np.unique([fl.split('/')[-1].split('.')[0] for fl in glob.glob('/Users/peterpfleiderer/Projects/git-packages/wlcalculator/data/cmip5_ver002/*')])

warming_levels = pd.DataFrame()
warming_levels['wlvl'] = np.arange(1,5.55,0.1)

cmipdata = CmipData.CmipData('CMIP5',models,['rcp26','rcp45','rcp60','rcp85'])
cmipdata.get_cmip(runs = ['r1i1p1'])
cmipdata.compute_period( [1986,2006], [1850,1900], warming_levels['wlvl'], window=21)
lvls=cmipdata.exceedance_tm

out = pd.DataFrame()
out['wlvl'] = lvls.wlevel
for rcp in lvls.scenario:
    out[rcp] = lvls[rcp].values.squeeze()
out.to_csv('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/climada_preprocessing/wlvls_all-rcps_cmip5_ensemble_21years.csv')








#
