import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader

os.chdir('/Users/peterpfleiderer/Projects/online_tools')

cous = shapereader.Reader('../data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
isos = [info.attributes['adm0_a3'] for info in cous]

wlvls = np.arange(1.0,5.0,0.1).round(1)

# rcp60 = pd.read_table('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/climada_preprocessing/wlvls_rcp60_cmip5_ensemble_21years.csv', sep=',')

avail_years = np.arange(2020,2105,5)
wlvl_inter = {}
for wlvl in wlvls:
	if wlvl in rcp60.wlvl.values.round(1):
		if int(rcp60.loc[rcp60.wlvl.values.round(1) == wlvl, 'rcp60'].values[0]) in avail_years:
			wlvl_inter[wlvl] = {'years':[int(rcp60.loc[rcp60.wlvl.values.round(1) == wlvl, 'rcp60'])], 'weight':[1]}
		else:
			year_deviations = np.abs(avail_years - int(rcp60.loc[rcp60.wlvl.values.round(1) == wlvl, 'rcp60'].values[0]))
			used_id = np.argsort(year_deviations)[:2]
			if np.max(year_deviations[used_id]) < 5:
				years = avail_years[used_id]
				deviations = year_deviations[used_id]
				wlvl_inter[wlvl] = {'years':years, 'weight':(1 / deviations) / (1 / deviations).sum() }

for iso in isos:
	print(iso)
	file_ = 'impact_data_explorer_data/climada_TC/csv/TropCyclone_Impact_'+iso+'.csv'
	if os.path.isfile(file_):
		
		raw = pd.read_table(file_, sep=',')[['years','aai_agg_rcp6.0','rp100_rcp6.0']]

		out_median = xr.DataArray(coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['region','wlvl','aggregation','season'])
		for wlvl,details in wlvl_inter.items():
			tmp = raw.loc[np.isin(raw.years,details['years']), 'aai_agg_rcp6.0'].values
			tmp = np.sum(tmp * details['weight'])
			out_median.loc[iso,str(wlvl),'other','annual'] = tmp

		xr.Dataset({'median':out_median, 'lower_bound':out_median, 'upper_bound':out_median}).to_netcdf('impact_data_explorer_data/climada_TC/ec3/'+'_'.join([iso,'ec3'])+'.nc')

		out_median = xr.DataArray(coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['region','wlvl','aggregation','season'])

		for wlvl,details in wlvl_inter.items():
			tmp = raw.loc[np.isin(raw.years,details['years']), 'rp100_rcp6.0'].values
			tmp = np.sum(tmp * details['weight'])
			out_median.loc[iso,str(wlvl),'other','annual'] = tmp

		xr.Dataset({'median':out_median, 'lower_bound':out_median, 'upper_bound':out_median}).to_netcdf('impact_data_explorer_data/climada_TC/ec4/'+'_'.join([iso,'ec4'])+'.nc')

	else:
		out_median = xr.DataArray(coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['region','wlvl','aggregation','season'])
		xr.Dataset({'median':out_median, 'lower_bound':out_median, 'upper_bound':out_median}).to_netcdf('impact_data_explorer_data/climada_TC/ec3/'+'_'.join([iso,'ec3'])+'.nc')	
		xr.Dataset({'median':out_median, 'lower_bound':out_median, 'upper_bound':out_median}).to_netcdf('impact_data_explorer_data/climada_TC/ec4/'+'_'.join([iso,'ec4'])+'.nc')	



#
