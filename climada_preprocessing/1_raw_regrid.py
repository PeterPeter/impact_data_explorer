import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import xesmf as xe

# here the command line arguments are analyzed
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v','--vars', help='variables', nargs='+', default=['ec3'])

parser.add_argument('--overwrite', dest='overwrite', action='store_true')
parser.add_argument('--no-overwrite', dest='overwrite', action='store_false')
parser.set_defaults(overwrite=True)

args = vars(parser.parse_args())
print(args)


# regrid to 720x360
ref_grid = xr.open_dataset('/p/tmp/pepflei/impact_data_explorer_data/wlvl_diff/tasAdjust/tasAdjust_1.5_1.4_annual.nc')['var']

for var in args['vars']:
    print(var)

    # load details about the variable
    sys.path.append('meta/variables/')
    exec("import %s; importlib.reload(%s); from %s import *" % tuple([var+'_check']*3))

    for fl in glob.glob(details['wlvl_files_pattern']):

        out_file = fl.replace(var, var+'_720x360')
        out_path = '/'.join(out_file.split('/')[:-1])
        if os.path.isdir(out_path) == False:
            os.system('mkdir -p '+out_path)

        if os.path.isfile(out_file) == False or args['overwrite']:
            data = xr.open_dataset(fl)[var]

            if 'regridder' not in globals():
                regridder = xe.Regridder(data, ref_grid, 'bilinear', reuse_weights=True)

            data_regrid = regridder(data)

            out_file = fl.replace(var, var+'_720x360')
            out_path = '/'.join(out_file.split('/')[:-1])
            if os.path.isdir(out_path) == False:
                os.system('mkdir -p '+out_path)
            xr.Dataset({var+'_720x360':regridder(data)}).to_netcdf(out_file)

            del data, data_regrid 
            gc.collect()

