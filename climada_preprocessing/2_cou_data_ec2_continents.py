import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader


import statsmodels.formula.api as smf

os.chdir('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer')


wlvl_year = pd.read_table('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/meta/warming_lvls_cmip5_21_years.csv', sep=',')
wlvl_year.wlvl = wlvl_year.wlvl.round(1)

data = pd.read_table('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/climada_flood/assembled_impact_ngfs_c.csv', sep=',')

cous = shapereader.Reader('../../data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
continents = np.unique([info.attributes['continent'] for info in cous])

continent_dict = {'GLOBAL':np.unique(data.Country)}
for continent in continents:
    print(continent)
    cous = shapereader.Reader('../../data/shapefiles/world/ne_50m_admin_0_countries.shp').records()
    isos = np.unique([info.attributes['adm0_a3'] for info in cous if info.attributes['continent']==continent])
    isos = isos[np.isin(isos,np.unique(data.Country))]
    if continent == 'Europe':
        isos = [iso for iso in isos if iso != 'RUS']

    continent_dict[continent] = isos

for continent,isos in continent_dict.items():
    print(continent)
    tmp = data.loc[np.isin(data.Country,isos)]
    for gcm in np.unique(tmp.GCM):
        print(gcm)
        for ghm in np.unique(tmp.GHM):
            for year in np.unique(tmp.Year):
                tmp_ = tmp.loc[(tmp.GCM==gcm) & (tmp.GHM==ghm) & (tmp.Year==year)].sum().to_dict()
                tmp_['GCM'] = gcm
                tmp_['GHM'] = ghm
                tmp_['Year'] = year
                tmp_['Country'] = continent
                data = data.append(tmp_, ignore_index=True)

data.to_csv('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/climada_flood/assembled_impact_ngfs_c_continents.csv')

