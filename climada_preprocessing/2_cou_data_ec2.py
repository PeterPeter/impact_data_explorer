import os,sys, importlib, glob,gc

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)

import numpy as np
import xarray as xr
import pandas as pd
import json
import cartopy.io.shapereader as shapereader


import statsmodels.formula.api as smf

os.chdir('/Users/peterpfleiderer/Projects/online_tools')


wlvl_year = pd.read_table('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer/meta/warming_lvls_cmip5_21_years.csv', sep=',')
wlvl_year.wlvl = wlvl_year.wlvl.round(1)

data = pd.read_table('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/climada_flood/assembled_impact_ngfs_c.csv', sep=',')

data.Country = [cou.replace(' ','_').upper() for cou in data.Country]

ims = np.unique(data.GHM)
gcms = np.unique(data.GCM)
gcm_dict = {'gfdl-esm2m':'GFDL-ESM2M', 'hadgem2-es':'HadGEM2-ES', 'ipsl-cm5a-lr':'IPSL-CM5A-LR', 'miroc5':'MIROC5'}
rcps = ['rcp26','rcp60','rcp85']
wlvls = np.arange(1.0,5.0,0.1).round(1)

for iso in [cou for cou in np.unique(data.Country) if len(cou)!=3]:
    print(iso)
    vals = xr.DataArray(coords={'gcm':gcms, 'im':ims, 'rcp':rcps, 'wlvl':wlvls}, dims=['gcm','rcp','im','wlvl'])
    tmp = data.loc[(data.Country == iso)]
    for wlvl in  wlvls:
        for gcm in gcms:
            for rcp in rcps:
                year = float(wlvl_year.loc[wlvl_year.wlvl == wlvl, gcm_dict[gcm]+'_'+rcp])
                if np.isfinite(year):
                    for im in ims:
                        vals.loc[gcm,rcp,im,wlvl] = np.nanmean(tmp.loc[(tmp.GCM == gcm) & (tmp.GHM == im) & np.isin(tmp.Year, np.arange(year-10,year+11)), 'damage_'+rcp])

    tmp = vals.loc[:,:,:,'1.1'].stack(ID=('gcm','im')).mean('rcp')
    tmp = tmp.assign_coords(ID = [g+'_'+i for g,i in zip(tmp.ID.gcm.values,tmp.ID.im.values)])
    ref_av = xr.DataArray(coords={'region':[iso], 'aggregation':['other'], 'season':['annual'], 'ID':tmp.ID}, dims=['region','aggregation','season','ID'])
    ref_av.loc[iso,'other','annual',:] = tmp
    xr.Dataset({'ref':ref_av}).to_netcdf('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/climada_flood/ec2/'+'_'.join([iso,'ec2'])+'_ref.nc')
    print(iso,float(tmp.mean()),float(tmp.median()))
    vals = (vals - vals.loc[:,:,:,'1.1']) / vals.loc[:,:,:,'1.1'] * 100

    gcm_im = vals.stack(gcm_im=('gcm','im')).mean('rcp')
    tmp_median = gcm_im.median('gcm_im')
    devs = gcm_im - tmp_median
    out_median = xr.DataArray(coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['wlvl','region','aggregation','season'])
    out_median.values = np.expand_dims(np.expand_dims(np.expand_dims(tmp_median.values, axis=-1), axis=-1), axis=-1)

    # filter out gcm/im combinations that only exist in ref
    devs = devs[:,np.isfinite(devs.loc[2].values)]
    # take only warming levels for which all gcm/im are available
    devs = devs[np.isfinite(devs.values.mean(axis=1)),:]
    if len(devs) != 0:
        tmp = pd.DataFrame()
        tmp['wlvl'] = np.repeat(np.array(devs.wlvl.values, np.float), devs.shape[1], 0)
        tmp['dev'] = devs.values.flatten()

        lower_bound = xr.DataArray(coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['wlvl','region','aggregation','season'])
        upper_bound = xr.DataArray(coords={'region':[iso], 'wlvl':[str(w) for w in wlvls], 'aggregation':['other'], 'season':['annual']}, dims=['wlvl','region','aggregation','season'])

        mod = smf.quantreg('dev ~ wlvl', tmp)
        for q,bound in zip([0.95,0.05],[upper_bound,lower_bound]):
            res = mod.fit(q=q)
            # get a line for the quantile regression for all warming levels
            slope = np.array(bound.wlvl.values, np.float) * res.params[1] + res.params[0]
            # check if this line crosses zero #or if the fit is horrible
            if (slope.min() < 0 and slope.max() > 0):# or res.pvalues.wlvl > 0.1:
                # if that is the case just take a constant quantile
                print('crossing',iso,'other','annual', slope.min(), slope.max(), res.pvalues.wlvl)
                bound.loc[:,iso,'other','annual'] = np.nanpercentile(devs, q*100)
            else:
                # otherwise store the slope
                bound.loc[:,iso,'other','annual'] = np.array(bound.wlvl.values, np.float) * res.params[1] + res.params[0]


        xr.Dataset({'median':out_median, 'lower_bound':lower_bound, 'upper_bound':upper_bound}).to_netcdf('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/climada_flood/ec2/'+'_'.join([iso,'ec2'])+'.nc')
        print(iso)
        print('/Users/peterpfleiderer/Projects/online_tools/impact_data_explorer_data/climada_flood/ec2/'+'_'.join([iso,'ec2'])+'.nc')




#
